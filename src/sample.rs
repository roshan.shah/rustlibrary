use std::{collections::HashMap, fmt::Display};
use core::{hash::Hash, num};
use rand::{thread_rng,Rng};
use crate::{finite_stochastic_language::FiniteStochasticLanguage, fraction::{one, zero, Fraction}, fuzzy_semantics::{AFuzzySemantics, FuzzySemantics}, stochastic_labelled_petri_net_semantics::StochasticLabelledPetriNetSemantics, stochastic_semantics::StochasticSemantics, Trace};
use anyhow::{anyhow, Result};
use num_traits::Zero;
use priority_queue::PriorityQueue;

pub trait FuzzySemanticsSampler {
    fn sample_minimum_probability(&self, at_least: &Fraction) -> Result<FiniteStochasticLanguage>;
    
    fn sample_number_of_traces(&self, number_of_traces: &usize) -> Result<FiniteStochasticLanguage>;

    fn sample_min_prob<FS: Hash + Display + Clone + Eq>(semantics: &dyn FuzzySemantics<FuzzyState = FS>, at_least: &Fraction) -> Result<FiniteStochasticLanguage>;

    fn sample_num_traces<FS: Hash + Display + Clone + Eq>(semantics: &dyn FuzzySemantics<FuzzyState = FS>, number_of_traces: &usize) -> Result<FiniteStochasticLanguage>;

    fn normalise(map: &mut HashMap<Vec<String>, Fraction>) {
        let sum = map.values().fold(zero(), |x, y| &x + y);
    
        log::info!("the sample covers {} probability mass", sum);
        map.retain(|_, v| {*v /= &sum;true});
    }
}


impl FuzzySemanticsSampler for AFuzzySemantics {
    fn sample_minimum_probability(&self, at_least: &Fraction) -> Result<FiniteStochasticLanguage> {
        match self {
            AFuzzySemantics::Usize(s) => {
                let semantics = s.as_ref();
                Self::sample_min_prob(semantics, at_least)
            },
            AFuzzySemantics::FuzzyMarking(s) => {
                let semantics = s.as_ref();

                // {
                //     let mut state = semantics.get_initial_state()?;
                //     for _ in 0..100 {
                //         state = semantics.execute_activity(&state, 0)?;
                //         for x in state.fuzzy_marking.iter() {
                //             if x.0.marking.get_place2token().get(1) == Some(&1) {
                //                 println!("{} {}", x.1, x.0);
                //             }
                //         }
                //     }
                // }

                Self::sample_min_prob(semantics, at_least)
            },
        }
    }

    fn sample_number_of_traces(&self, number_of_traces: &usize) -> Result<FiniteStochasticLanguage> {
        match self {
            AFuzzySemantics::Usize(s) => {
                let semantics = s.as_ref();
                Self::sample_num_traces(semantics, number_of_traces)
            },
            AFuzzySemantics::FuzzyMarking(s) => {
                let semantics = s.as_ref();
                Self::sample_num_traces(semantics, number_of_traces)
            },
        }
    }

    fn sample_min_prob<FS: Hash + Display + Clone + Eq>(semantics: &dyn FuzzySemantics<FuzzyState = FS>, at_least: &Fraction) -> Result<FiniteStochasticLanguage> {
        let mut result = HashMap::new();

        let mut queue = vec![];
        queue.push(X{
            prefix: vec![],
            probability: one(),
            fuzzy_state: semantics.get_initial_state()?,
        });

        while let Some(x) = queue.pop() {
            let termination_probability = &x.probability * semantics.get_termination_probability(&x.fuzzy_state);
            if termination_probability >= *at_least{
                result.insert(x.prefix.clone(), termination_probability);
            }

            if &x.probability * (one() - semantics.get_termination_probability(&x.fuzzy_state)) >= *at_least {
                for activity in semantics.get_enabled_activities(&x.fuzzy_state) {
                    let probability = semantics.get_activity_probability(&x.fuzzy_state, activity);

                    let new_fuzzy_state = semantics.execute_activity(&x.fuzzy_state, activity)?;
                    let mut new_prefix = x.prefix.clone();
                    new_prefix.push(semantics.get_activity_label(activity).to_string());
                    let new_x = X {
                        prefix: new_prefix,
                        probability: &x.probability * probability,
                        fuzzy_state: new_fuzzy_state,
                    };

                    queue.push(new_x);
                }
            }
        }

        if result.is_empty() {
            return Err(anyhow!("Sampling returned an empty language; there are no traces that have a probability of at least {}.", at_least));
        }
        let sum = result.values().fold(zero(), |x, y| &x + y);
        Ok(FiniteStochasticLanguage::from(result))
    }

    fn sample_num_traces<FS: Hash + Display + Clone + Eq>(semantics: &dyn FuzzySemantics<FuzzyState = FS>, number_of_traces: &usize) -> Result<FiniteStochasticLanguage> {
        let mut result = HashMap::new();

        let mut queue = PriorityQueue::new();
        queue.push(Y::Prefix(vec![], semantics.get_initial_state()?), one());

        while let Some((y, y_probability)) = queue.pop() {
            match y {
                Y::Prefix(prefix, fuzzy_state) => {
                    log::info!("fuzzy state {}", fuzzy_state);
                    let termination_proability = semantics.get_termination_probability(&fuzzy_state);
                    if !termination_proability.is_zero() {
                        queue.push(Y::Trace(prefix.clone()), &y_probability * termination_proability);
                    }

                    for activity in semantics.get_enabled_activities(&fuzzy_state) {
                        let activity_probability = semantics.get_activity_probability(&fuzzy_state, activity);
        
                        let new_fuzzy_state = semantics.execute_activity(&fuzzy_state, activity)?;
                        let mut new_prefix = prefix.clone();
                        new_prefix.push(semantics.get_activity_label(activity).to_string());
                        let new_probability = &y_probability * activity_probability;
        
                        queue.push(Y::Prefix(new_prefix, new_fuzzy_state), new_probability);
                    }
                },
                Y::Trace(trace) => {
                    result.insert(trace, y_probability);
                    if result.len() >= *number_of_traces {
                        break;
                    }
                },
            }
        }

        if result.is_empty() {
            return Err(anyhow!("Sampling returned an empty language; there are no traces in the model."));
        }
        let sum = result.values().fold(zero(), |x, y| &x + y);
        Ok(FiniteStochasticLanguage::from(result))
        
        }
    }


#[derive(Debug,Clone)]
pub struct Prob {
    outgoing_states: Vec<usize>,
    outgoing_state_probabilities: Vec<Fraction>
}


struct X<FS: Hash + Display + Clone + Eq> {
    prefix: Trace,
    probability: Fraction,
    fuzzy_state: FS
}

enum Y<FS: Hash + Display + Clone + Eq> {
    Prefix(Trace, FS),
    Trace(Trace)
}

impl <FS: Hash + Display + Clone + Eq> Eq for Y<FS> {}

impl <FS: Hash + Display + Clone + Eq> PartialEq for Y<FS> {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Prefix(l0, l1), Self::Prefix(r0, r1)) => l0 == r0,
            (Self::Trace(l0), Self::Trace(r0)) => l0 == r0,
            _ => false,
        }
    }
}

impl <FS: Hash + Display + Clone + Eq> Hash for Y<FS> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        match self {
            Y::Prefix(t, _) => t.hash(state),
            Y::Trace(t) => t.hash(state),
        }
    }
}


pub fn choose_randomly(probabilities: &[Fraction]) -> usize {
    
    let mut total = zero();
    for i in probabilities.iter(){
        total += i;
    }
    let mut rng = rand::thread_rng();
    let mut cum_prob= zero();
    let rand_val: f64 = rng.gen_range(0.0,1.0);

    for (index, &ref value) in probabilities.iter().enumerate() {
        let trans_prob = value.clone();
        cum_prob = cum_prob + trans_prob;
        if Fraction::from(rand_val) < cum_prob {
            return index;
        }
    }
    probabilities.len() - 1 
}

