use std::{collections::HashSet, fmt::{Display, Pointer}, hash::Hash, path::PathBuf};
use anyhow::Result;

use clap::{builder::ValueParser, value_parser};

use crate::{ebi_commands::{ebi_command::{EbiCommand2, EBI_COMMANDS}, ebi_command_info::Infoable}, ebi_objects::ebi_object::{EbiObject, EbiObjectType, EbiTraitObject}, ebi_traits::ebi_trait::{EbiTrait, FromEbiTraitObject}, export::Exportable, file_handler::{EbiFileHandler, EBI_FILE_HANDLERS}, math::{fraction::Fraction, log_div::LogDiv, root::ContainsRoot}};

#[derive(PartialEq,Eq)]
pub enum EbiInputType {
    Trait(EbiTrait),
    ObjectType(EbiObjectType),
    AnyObject,
    FileHandler,
    String,
    Usize,
    Fraction,
}

impl EbiInputType{

    pub fn get_article(&self) -> &str {
        match self {
            EbiInputType::Trait(t) => t.get_article(),
            EbiInputType::ObjectType(o) => o.get_article(),
            EbiInputType::AnyObject => "an",
            EbiInputType::String => "a",
            EbiInputType::Usize => "an",
            EbiInputType::FileHandler => "a",
            EbiInputType::Fraction => "a",
        }
    }

    pub fn get_parser_of_list(traits: &[&EbiInputType]) -> ValueParser {
        match traits[0] {
            EbiInputType::Trait(_) => value_parser!(PathBuf),
            EbiInputType::ObjectType(_) => value_parser!(PathBuf),
            EbiInputType::AnyObject => value_parser!(PathBuf),
            EbiInputType::String => value_parser!(String).into(),
            EbiInputType::Usize => value_parser!(usize).into(),
            EbiInputType::FileHandler => value_parser!(EbiFileHandler).into(),
            EbiInputType::Fraction => value_parser!(Fraction).into(),
        }
    }

    pub fn get_possible_inputs(traits: &[&EbiInputType]) -> String {
        let mut result = HashSet::new();

        for input_type in traits {
            match input_type {
                EbiInputType::Trait(t) => {
                    result.extend(t.get_file_handlers().iter().map(|file_type| file_type.file_extension.to_string()));
                },
                EbiInputType::ObjectType(o) => {result.insert(o.to_string());},
                EbiInputType::AnyObject => {
                    result.extend(EBI_FILE_HANDLERS.iter().map(|file_type| file_type.file_extension.to_string()));
                },
                EbiInputType::String => {result.insert("text".to_string());},
                EbiInputType::Usize => {result.insert("integer".to_string());},
                EbiInputType::FileHandler => {
                    let extensions: Vec<&str> = EBI_FILE_HANDLERS.iter().map(|file_type| file_type.file_extension).collect();
                    result.insert("any file type supported by Ebi (".to_owned() + &extensions.join(", ") + ")");
                },
                EbiInputType::Fraction => {result.insert("fraction".to_string());},
            };
        }

        let result = result.into_iter().collect::<Vec<_>>();
        return result.join(", ");
    }
    
    pub fn get_applicable_commands(&self) -> HashSet<Vec<&'static EbiCommand2>> {
        let mut result = HashSet::new();
        self.get_applicable_commands_recursive(&EBI_COMMANDS, &mut result, vec![]);
        result
    }

    pub fn get_applicable_commands_recursive(&self, command: &'static EbiCommand2, result: &mut HashSet<Vec<&'static EbiCommand2>>, prefix: Vec<&'static EbiCommand2>) {
        match command {
            EbiCommand2::Group { name_short, explanation_short, explanation_long, children } => {
                for child in children.iter() {
                    let mut prefix = prefix.clone();
                    prefix.push(command);
                    self.get_applicable_commands_recursive(child, result, prefix);
                }
            },
            EbiCommand2::Command { name_short, name_long, explanation_short, explanation_long, latex_link, cli_command, exact_arithmetic, input_types, input_names, input_helps, execute, output } => {
                for input_typess in input_types.iter() {
                    for input_typesss in input_typess.iter() {
                        if input_typesss == &self {
                            let mut prefix = prefix.clone();
                            prefix.push(command);
                            result.insert(prefix);
                        }
                    }
                }
            },
        }   
    }
}

impl Display for EbiInputType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            EbiInputType::Trait(t) => t.fmt(f),
            EbiInputType::ObjectType(o) => o.fmt(f),
            EbiInputType::AnyObject => write!(f, "object"),
            EbiInputType::String => write!(f, "text"),
            EbiInputType::Usize => write!(f, "integer"),
            EbiInputType::FileHandler => write!(f, "file"),
            EbiInputType::Fraction => write!(f, "fraction"),
        }
    }
}

pub enum EbiInput {
    Trait(EbiTraitObject),
    Object(EbiObject),
    String(String),
    Usize(usize),
    FileHandler(EbiFileHandler),
    Fraction(Fraction),
}

impl EbiInput {
    pub fn to_type<T: FromEbiTraitObject + ?Sized>(self) -> Result<Box<T>> {
        FromEbiTraitObject::from_trait_object(self)
    }

    pub fn get_type(&self) -> EbiInputType {
        match self {
            EbiInput::Trait(t) => EbiInputType::Trait(t.get_trait()),
            EbiInput::Object(o) => EbiInputType::ObjectType(o.get_type()),
            EbiInput::String(_) => EbiInputType::String,
            EbiInput::Usize(_) => EbiInputType::Usize,
            EbiInput::FileHandler(_) => EbiInputType::FileHandler,
            EbiInput::Fraction(_) => EbiInputType::Fraction,
        }
    }
}


#[derive(PartialEq,Eq)]
pub enum EbiOutputType {
    ObjectType(EbiObjectType),
    String,
    Usize,
    Fraction,
    LogDiv,
    ContainsRoot
}

impl EbiOutputType {
    pub fn get_suggested_extension(&self) -> &str {
        match self {
            EbiOutputType::ObjectType(o) => o.get_suggested_extension(),
            EbiOutputType::String => "txt",
            EbiOutputType::Usize => "int",
            EbiOutputType::Fraction => "fra",
            EbiOutputType::LogDiv => "log",
            EbiOutputType::ContainsRoot => "root",
        }
    }
}

impl Display for EbiOutputType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            EbiOutputType::ObjectType(t) => t.fmt(f),
            EbiOutputType::String => "text".fmt(f),
            EbiOutputType::Usize => "integer".fmt(f),
            EbiOutputType::Fraction => "fraction".fmt(f),
            EbiOutputType::LogDiv => "logarithm".fmt(f),
            EbiOutputType::ContainsRoot => "root".fmt(f),
        }
    }
}

pub enum EbiOutput {
    Object(EbiObject),
    String(String),
    Usize(usize),
    Fraction(Fraction),
    LogDiv(LogDiv),
    ContainsRoot(ContainsRoot)
}

impl EbiOutput {
    pub fn get_type(&self) -> EbiOutputType {
        match self {
            EbiOutput::Object(o) => EbiOutputType::ObjectType(o.get_type()),
            EbiOutput::String(_) => EbiOutputType::String,
            EbiOutput::Usize(_) => EbiOutputType::Usize,
            EbiOutput::Fraction(_) => EbiOutputType::Fraction,
            EbiOutput::LogDiv(_) => EbiOutputType::LogDiv,
            EbiOutput::ContainsRoot(_) => EbiOutputType::ContainsRoot,
        }
    }
}

impl Exportable for EbiOutput {
    fn export(&self, f: &mut impl std::io::Write) -> Result<()> {
        match self {
            EbiOutput::Object(o) => o.export(f),
            EbiOutput::String(s) => s.export(f),
            EbiOutput::Usize(u) => u.export(f),
            EbiOutput::Fraction(fr) => fr.export(f),
            EbiOutput::LogDiv(l) => l.export(f),
            EbiOutput::ContainsRoot(c) => c.export(f),
        }
    }
}