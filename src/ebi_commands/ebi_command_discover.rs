use std::{path::PathBuf, io::{self, IsTerminal}};
use anyhow::{anyhow, Result, Context};
use clap::{Command, arg, value_parser, ArgMatches, ArgAction, Arg};

use crate::{ebi_input_output::{EbiInput, EbiInputType, EbiOutput, EbiOutputType}, ebi_objects::{ebi_object::{EbiObject, EbiObjectType, EbiTraitObject}, labelled_petri_net::{LabelledPetriNet, EBI_LABELLED_PETRI_NET}}, ebi_traits::{ebi_trait::EbiTrait, ebi_trait_finite_stochastic_language::EbiTraitFiniteStochasticLanguage, ebi_trait_labelled_petri_net::EbiTraitLabelledPetriNet}, export, import, occurrences_miner, uniform_stochastic_miner::{self, uniform_stochastic_miner}};
use super::ebi_command::EbiCommand2;

pub const EBI_DISCOVER: EbiCommand2 = EbiCommand2::Group {
    name_short: "discover",
    explanation_short: "Discover a process model.",
    explanation_long: None,
    children: &[
        &EBI_DISCOVER_OCCURRENCE,
        &EBI_DISCOVER_UNIFORM
    ],
};

pub const EBI_DISCOVER_OCCURRENCE: EbiCommand2 = EbiCommand2::Command { 
    name_short: "occurrence", 
    name_long: None, 
    explanation_short: "Give each transition a weight that matches the occurrences of its label; silent transitions get a weight of 1.", 
    explanation_long: None, 
    latex_link: Some("~\\cite{DBLP:conf/icpm/BurkeLW20}"), 
    cli_command: None, 
    exact_arithmetic: true, 
    input_types: &[ 
        &[ &EbiInputType::Trait(EbiTrait::FiniteStochasticLanguage)], 
        &[ &EbiInputType::Trait(EbiTrait::LabelledPetriNet)] 
    ], 
    input_names: &[ "FILE_1", "FILE_2" ], 
    input_helps: &[ "A finite stochastic language (log) to get the occurrences from.", "A labelled Petri net with the control flow." ], 
    execute: |mut inputs, _| {
        let language = inputs.remove(0).to_type::<dyn EbiTraitFiniteStochasticLanguage>()?;
        let lpn = inputs.remove(0).to_type::<dyn EbiTraitLabelledPetriNet>()?;
        Ok(EbiOutput::Object(EbiObject::StochasticLabelledPetriNet(occurrences_miner::mine(lpn, language))))
    }, 
    output: &EbiOutputType::ObjectType(EbiObjectType::StochasticLabelledPetriNet)
};

pub const EBI_DISCOVER_UNIFORM: EbiCommand2 = EbiCommand2::Command { 
    name_short: "uniform", 
    name_long: None, 
    explanation_short: "Give each transition a weight of 1.", 
    explanation_long: None, 
    latex_link: None, 
    cli_command: None, 
    exact_arithmetic: true, 
    input_types: &[ 
        &[ &EbiInputType::Trait(EbiTrait::LabelledPetriNet)] 
    ], 
    input_names: &["LPN_FILE" ], 
    input_helps: &[ "A labelled Petri net." ], 
    execute: |mut inputs, _| {
        let lpn = inputs.remove(0).to_type::<dyn EbiTraitLabelledPetriNet>()?;
        Ok(EbiOutput::Object(EbiObject::StochasticLabelledPetriNet(uniform_stochastic_miner::uniform_stochastic_miner(lpn))))
    }, 
    output: &EbiOutputType::ObjectType(EbiObjectType::StochasticLabelledPetriNet)
};