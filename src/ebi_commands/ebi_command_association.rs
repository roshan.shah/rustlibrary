use std::{io::{self, IsTerminal, Write}, path::PathBuf};

use clap::{value_parser, Arg, ArgAction, ArgMatches, Command};
use anyhow::{anyhow, Context, Result};
use fraction::One;
use crate::{association, ebi_info, ebi_input_output::{EbiInputType, EbiOutput, EbiOutputType}, ebi_objects::ebi_object::{EbiObject, EbiObjectType}, ebi_traits::{ebi_trait::EbiTrait, ebi_trait_event_log::EbiTraitEventLog}, import, math::fraction::Fraction};

use super::ebi_command::EbiCommand2;

macro_rules! number_of_samples {
    () => {500};
}

pub const EBI_ASSOCIATION: EbiCommand2 = EbiCommand2::Group {
    name_short: "association", 
    explanation_short: "Compute associations between a process and other aspects.", 
    explanation_long: None, 
    children: &[
        &ASSOCIATION_ATTRIBUTE,
        &ASSOCIATION_ATTRIBUTES
    ]
};

pub const ASSOCIATION_ATTRIBUTE: EbiCommand2 = EbiCommand2::Command { 
    name_short: "attribute", 
    name_long: Some("trace-attribute"),
    explanation_short: "Compute the association between the process and a trace attribute.", 
    explanation_long: Some(concat!("Compute the association between the process and a given trace attribute; ", number_of_samples!(), " samples are taken.")), 
    latex_link: Some("\\cite{DBLP:journals/tkde/LeemansMPH23}"), 
    cli_command: Some(|command| cli_number_of_samples(command)), 
    exact_arithmetic: true, 
    input_types: &[ 
        &[&EbiInputType::Trait(EbiTrait::EventLog)], 
        &[&EbiInputType::String] 
    ], 
    input_names: &[ "FILE", "ATTRIBUTE" ], 
    input_helps: &[ "The event log for which association is to be computed.", concat!(concat!("The trace attribute for which association is to be computed. The trace attributes of a log can be found using `Ebi ", ebi_info!()), "`.")], 
    execute: |mut inputs, cli_matches| {
        let mut event_log = inputs.remove(0).to_type::<dyn EbiTraitEventLog>()?;
        let attribute = inputs.remove(0).to_type::<String>()?;
        let number_of_samples = cli_matches.get_one::<usize>("samples").unwrap();

        let ass = association::association(&mut event_log, *number_of_samples, &attribute).with_context(|| format!("attribute {}", attribute))?;

        Ok(EbiOutput::ContainsRoot(ass))
    }, 
    output: &EbiOutputType::ContainsRoot
};

pub const ASSOCIATION_ATTRIBUTES: EbiCommand2 = EbiCommand2::Command { 
    name_short: "attributes", 
    name_long: Some("all-trace-attributes"),
    explanation_short: "Compute the association between the process and trace attributes.", 
    explanation_long: Some(concat!("Compute the association between the process and trace attributes; ", number_of_samples!(), " samples are taken.")), 
    latex_link: Some("\\cite{DBLP:journals/tkde/LeemansMPH23}"), 
    cli_command: Some(|command| cli_number_of_samples(command)), 
    exact_arithmetic: true, 
    input_types: &[ 
        &[&EbiInputType::Trait(EbiTrait::EventLog)], 
     ], 
    input_names: &[ "FILE" ], 
    input_helps: &[ "The event log for which association is to be computed." ], 
    execute: |mut inputs, cli_matches| {
        let mut event_log = inputs.remove(0).to_type::<dyn EbiTraitEventLog>()?;
        let number_of_samples = cli_matches.get_one::<usize>("samples").unwrap();

        let result = association::associations(&mut event_log, *number_of_samples);

        let mut f = vec![];
        for (x, y) in result {
            let y = y?;
            writeln!(f, "Trace atribute `{}` has an association of approximately {}. Exact value: {:?}", x, &y.approximate(), &y);
        }
        Ok(EbiOutput::String(String::from_utf8(f).unwrap()))
    }, 
    output: &EbiOutputType::String
};

fn cli_number_of_samples(command: Command) -> Command {
    command.arg(
        Arg::new("samples")
        .action(ArgAction::Set)
        .value_name("NUMBER")
        .short('s')
        .long("number-of-samples")
        .help("Take a number of samples.")
        .default_value(number_of_samples!().to_string())
        .value_parser(value_parser!(usize))
        .required(false)
    )
}

fn compute_association(matches: &ArgMatches) -> Result<()> {
    let mut reader = import::get_reader(matches, "event_log", true).context("could not open source file")?;
    let mut event_log = import::import_event_log(&mut reader).context("could not open event log")?;
    
    if let Some(attribute) = matches.get_one::<String>("attribute") {
        log::info!("only attribute {}", attribute);
        let ass = association::association(&mut event_log, number_of_samples!(), attribute).with_context(|| format!("attribute {}", attribute))?;

        println!("approximately {} ({:?})", ass.approximate(), ass);
    } else {
        let result = association::associations(&mut event_log, number_of_samples!());

        for (x, y) in result {
            if let Ok(ass) = y {
                println!("Trace atribute `{}` has an association of approximately {}. Exact value: {:?}", x, ass.approximate(), ass);
            } else {
                println!("For trace attribute `{}` no association could be computed, as {}", x, y.err().unwrap());
            }
        }
    }
    
    return Ok(());
}