use std::{path::PathBuf, io::{self, IsTerminal}};
use clap::{Command, ArgMatches, value_parser, Arg, ArgAction};
use anyhow::{Result, Context, anyhow};

use crate::{ebi_input_output::{EbiInput, EbiInputType, EbiOutput, EbiOutputType}, ebi_objects::ebi_object::{EbiObject, EbiObjectType, EbiTraitObject}, ebi_traits::{ebi_trait::EbiTrait, ebi_trait_finite_stochastic_language::EbiTraitFiniteStochasticLanguage, ebi_trait_queriable_stochastic_language::EbiTraitQueriableStochasticLanguage}, entropic_relevance, export, import, jenson_shannon_stochastic_conformance, math::{fraction::Fraction, log_div::LogDiv}, unit_earth_movers_stochastic_conformance};

use super::ebi_command::EbiCommand2;

pub const EBI_CONFORMANCE: EbiCommand2 = EbiCommand2::Group { 
    name_short: "conformance", 
    explanation_short: "Check the conformance of two stochastic languages.", 
    explanation_long: None, 
    children: &[
        &CONFORMANCE_ER,
        &CONFORMANCE_JSSC,
        &CONFORMANCE_JSSC_SAMPLE,
        &CONFORMANCE_UEMSC,
    ]
};

pub const CONFORMANCE_UEMSC: EbiCommand2 = EbiCommand2::Command {
    name_short: "uemsc",
    name_long: Some("unit-earth-movers-stochastic-conformance"), 
    explanation_short: "Compute unit-earth movers' stochastic conformance.", 
    explanation_long: None, 
    latex_link: Some("\\cite{DBLP:conf/bpm/LeemansSA19}"), 
    cli_command: None, 
    exact_arithmetic: true, 
    input_types: &[ 
        &[&EbiInputType::Trait(EbiTrait::FiniteStochasticLanguage)], 
        &[&EbiInputType::Trait(EbiTrait::QueriableStochasticLanguage)]
    ], 
    input_names: &["FILE_1", "FILE_2"], 
    input_helps: &["A finite stochastic language (log) to compare.", "A queriable stochastic language (model) to compare."],
    execute: |mut inputs, _| {
        let log = inputs.remove(0).to_type::<dyn EbiTraitFiniteStochasticLanguage>()?;
        let model = inputs.remove(0).to_type::<dyn EbiTraitQueriableStochasticLanguage>()?;
        Ok(EbiOutput::Fraction(unit_earth_movers_stochastic_conformance::uemsc(log, model).context("cannot compute uEMSC")?))
    },
    output: &EbiOutputType::Fraction
};

pub const CONFORMANCE_ER: EbiCommand2 = EbiCommand2::Command { 
    name_short: "er", 
    name_long: Some("entropic-relevance"), 
    explanation_short: "Compute entropic relevance (uniform).", 
    explanation_long: None, 
    latex_link: Some("\\cite{DBLP:journals/is/AlkhammashPMG22}"), 
    cli_command: None, 
    exact_arithmetic: true,
    input_types: &[ 
        &[&EbiInputType::Trait(EbiTrait::FiniteStochasticLanguage)], 
        &[&EbiInputType::Trait(EbiTrait::QueriableStochasticLanguage)]
    ], 
    input_names: &["FILE_1", "FILE_2"], 
    input_helps: &["A finite stochastic language (log) to compare.", "A queriable stochastic language (model) to compare."],
    execute: |mut inputs, _| {
        let log = inputs.remove(0).to_type::<dyn EbiTraitFiniteStochasticLanguage>()?;
        let model = inputs.remove(0).to_type::<dyn EbiTraitQueriableStochasticLanguage>()?;
        Ok(EbiOutput::LogDiv(entropic_relevance::er(log, model).context("cannot compute uEMSC")?))
    }, 
    output: &EbiOutputType::LogDiv
};

pub const CONFORMANCE_JSSC: EbiCommand2 = EbiCommand2::Command { 
    name_short: "jssc", 
    name_long: Some("jensen-shannon-stochastic-conformance"), 
    explanation_short: "Compute Jensen-Shannon stochastic conformance.", 
    explanation_long: None, 
    latex_link: None, 
    cli_command: None, 
    exact_arithmetic: false, 
    input_types: &[ 
        &[ &EbiInputType::Trait(EbiTrait::FiniteStochasticLanguage)], 
        &[ &EbiInputType::Trait(EbiTrait::FiniteStochasticLanguage), &EbiInputType::Trait(EbiTrait::QueriableStochasticLanguage)] 
    ], 
    input_names: &["FILE_1", "FILE_2"], 
    input_helps: &["A finite stochastic language to compare.", "A queriable stochastic language to compare."],
    execute: |mut inputs, _| {
        let event_log = inputs.remove(0).to_type::<dyn EbiTraitFiniteStochasticLanguage>()?;

        match inputs.remove(0) {            
            EbiInput::Trait(EbiTraitObject::FiniteStochasticLanguage(slang)) => {
                Ok(EbiOutput::LogDiv(jenson_shannon_stochastic_conformance::jssc_log2log(event_log, slang).context("cannot compute JSSC")?))
            },
            EbiInput::Trait(EbiTraitObject::QueriableStochasticLanguage(slang)) => {
                Ok(EbiOutput::LogDiv(jenson_shannon_stochastic_conformance::jssc_log2model(event_log, slang).context("cannot compute JSSC")?))
            }
            _ => Err(anyhow!("wrong input given"))
        }
    },
    output: &EbiOutputType::LogDiv 
};

pub const CONFORMANCE_JSSC_SAMPLE: EbiCommand2 = EbiCommand2::Command { 
    name_short: "jssc-sample", 
    name_long: Some("jensen-shannon-stochastic-conformance-sample"), 
    explanation_short: "Compute Jensen-Shannon stochastic conformance with sampling.", 
    explanation_long: None, 
    latex_link: None, 
    cli_command: None, 
    exact_arithmetic: false, 
    input_types: &[ 
        &[ &EbiInputType::Trait(EbiTrait::QueriableStochasticLanguage)], 
        &[ &EbiInputType::Trait(EbiTrait::QueriableStochasticLanguage)], 
        &[ &EbiInputType::Usize ] 
    ], 
    input_names: &["FILE_1", "FILE_2"], 
    input_helps: &["A queriable stochastic language to compare.", "A queriable stochastic language to compare."],
    execute: |mut inputs, _| {
        let mut model1 = inputs.remove(0).to_type::<dyn EbiTraitQueriableStochasticLanguage>()?;
        let mut model2 = inputs.remove(0).to_type::<dyn EbiTraitQueriableStochasticLanguage>()?;
        let number_of_traces = inputs.remove(0).to_type::<usize>()?;

        Ok(EbiOutput::LogDiv(jenson_shannon_stochastic_conformance::jssc_model2model_with_random_sample(model1, model2, *number_of_traces).context("cannot compute JSSC")?))
    },
    output: &EbiOutputType::LogDiv 
};