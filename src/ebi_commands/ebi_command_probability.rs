use std::{path::PathBuf, io::{self, IsTerminal}};
use anyhow::{Result, Ok, Context, anyhow};
use clap::{arg, value_parser, Arg, ArgAction, ArgMatches, Command};
use fraction::{One, Zero};

use crate::{ebi_input_output::{EbiInputType, EbiOutput, EbiOutputType}, ebi_objects::ebi_object::{EbiObject, EbiObjectType}, ebi_traits::{ebi_trait::EbiTrait, ebi_trait_finite_language::EbiTraitFiniteLanguage, ebi_trait_finite_stochastic_language::EbiTraitFiniteStochasticLanguage, ebi_trait_queriable_stochastic_language::EbiTraitQueriableStochasticLanguage}, export, follower_semantics::FollowerSemantics, import, math::fraction::Fraction};

use super::ebi_command::EbiCommand2;

pub const EBI_PROBABILITY: EbiCommand2 = EbiCommand2::Group { 
    name_short: "probability", 
    explanation_short: "Compute the probability of a trace or specification on the model.", 
    explanation_long: None, 
    children: &[
        &EBI_PROBABILITY_MODEL,
        &EBI_PROBABILITY_TRACE
    ]
};

pub const EBI_PROBABILITY_MODEL: EbiCommand2 = EbiCommand2::Command { 
    name_short: "model", 
    name_long: None, 
    explanation_short: "Compute the probability that a queriable stochastic language (stochastic model) produces any trace of the model.", 
    explanation_long: None, 
    latex_link: Some("~\\cite{DBLP:journals/is/LeemansMM24}"), 
    cli_command: None, 
    exact_arithmetic: true, 
    input_types: &[ 
        &[ &EbiInputType::Trait(EbiTrait::QueriableStochasticLanguage) ], 
        &[ &EbiInputType::Trait(EbiTrait::FiniteLanguage) ] 
    ], 
    input_names: &[ "FILE_1", "FILE_2" ], 
    input_helps: &[ "The queriable stochastic language (model).", "The finite language (log)." ], 
    execute: |mut inputs, cli_matches| {
        let mut model: Box<dyn EbiTraitQueriableStochasticLanguage> = inputs.remove(0).to_type::<dyn EbiTraitQueriableStochasticLanguage>()?;
        let mut log = inputs.remove(0).to_type::<dyn EbiTraitFiniteLanguage>()?;
        
        let mut sum = Fraction::zero();
        for trace in log.iter() {
            sum += model.get_probability(&FollowerSemantics::Trace(&trace)).with_context(|| format!("cannot compute probability of trace {:?}", trace))?;
        }
        return Ok(EbiOutput::Fraction(sum));
    }, 
    output: &EbiOutputType::Fraction,
};

pub const EBI_PROBABILITY_TRACE: EbiCommand2 = EbiCommand2::Command { 
    name_short: "trace", 
    name_long: None, 
    explanation_short: "Compute the probability of a trace in a queriable stochastic language (model).", 
    explanation_long: None, 
    latex_link: Some("~\\cite{DBLP:journals/is/LeemansMM24}"), 
    cli_command: Some(|command| {
        command.arg(Arg::new("trace")
            .action(ArgAction::Set)
            .value_name("TRACE")
            .help("The trace.")
            .required(true)
            .value_parser(value_parser!(String))
            .num_args(0..))
    }), 
    exact_arithmetic: true, 
    input_types: &[ 
        &[ &EbiInputType::Trait(EbiTrait::QueriableStochasticLanguage) ] 
    ], 
    input_names: &[ "FILE" ], 
    input_helps: &[ "The queriable stochastic language (model)." ], 
    execute: |mut inputs, cli_matches| {
        let mut model = inputs.remove(0).to_type::<dyn EbiTraitQueriableStochasticLanguage>()?;
        if let Some(x) = cli_matches.get_many::<String>("trace") {
            let t: Vec<&String> = x.collect();
            let trace = t.into_iter().cloned().collect();

            log::trace!("compute probability of trace {:?}", trace);
        
            let result = model.get_probability(&FollowerSemantics::Trace(&trace)).with_context(|| format!("cannot compute probability of trace {:?}", trace))?;
            return Ok(EbiOutput::Fraction(result));
        } else {
            return Err(anyhow!("no trace given"));
        }
    }, 
    output: &EbiOutputType::Fraction,
};

fn probability(matches: &ArgMatches) -> Result<()> {
    let mut reader1 = import::get_reader(matches, "logmodel1", true).context("could not open first file")?;
    let mut logmodel1 = import::import_queriable_stochastic_language(&mut reader1)?;

    if let Some(matches) = matches.subcommand_matches("trace") {
        //compute probability of a trace

        if let Some(x) = matches.get_many::<String>("trace") {
            let t: Vec<&String> = x.collect();
            let trace = t.into_iter().cloned().collect();

            log::trace!("compute probability of trace {:?}", trace);
        
            let result = logmodel1.get_probability(&FollowerSemantics::Trace(&trace)).with_context(|| format!("cannot compute probability of trace {:?}", trace))?;
            export::output_fraction(&result);
        } else {
            return Err(anyhow!("no trace given"));
        }
    } else if let Some(matches) = matches.subcommand_matches("model") {
        //compute probability of a model
        let mut reader2 = import::get_reader(matches, "logmodel2", false).context("could not open second file")?;
        let mut logmodel2 = import::import_finite_language(&mut reader2).context("could not import second file")?;

        let mut sum = Fraction::zero();
        for trace in logmodel2.iter() {
            sum += logmodel1.get_probability(&FollowerSemantics::Trace(&trace)).with_context(|| format!("cannot compute probability of trace {:?}", trace))?;
        }
        export::output_fraction(&sum);
    }

    Ok(())
}