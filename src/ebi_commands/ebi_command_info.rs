use std::io::Write;
use anyhow::{anyhow, Result};
use clap::builder::Str;
use flate2::write;
use fraction::BigFraction;
use inflector::Inflector;

use crate::{ebi_input_output::{EbiInput, EbiInputType, EbiOutput, EbiOutputType}, ebi_objects::ebi_object::{EbiObject, EbiObjectType, EbiTraitObject}, ebi_traits::ebi_trait::EbiTrait};

use super::ebi_command::EbiCommand2;

#[macro_export]
macro_rules! ebi_info {
    () => {"info"};
}

pub const EBI_INFO: EbiCommand2 = EbiCommand2::Command { 
    name_short: ebi_info!(), 
    name_long: Some("information"), 
    explanation_short: "show information", 
    explanation_long: None, 
    latex_link: None, 
    cli_command: None, 
    exact_arithmetic: true, 
    input_types: &[ &[&EbiInputType::AnyObject] ], 
    input_names: &[ "FILE" ], 
    input_helps: &[ "Any file supported by Ebi." ], 
    execute: |mut inputs, cli_matches| {
        let mut f = vec![];

        if let EbiInput::Object(object) = inputs.remove(0) {
            writeln!(f, "Object was recognised as {} {}.", object.get_type().get_article(), object.get_type());

            //object-specific info
            object.info(&mut f)?;

            //show applicable commands
            let commands = object.get_type().get_applicable_commands();
            if commands.is_empty() {
                writeln!(f, "{} {} is not an input for any Ebi command.", object.get_type().get_article().to_string().to_sentence_case(), object.get_type())?;
            } else {
                let paths: Vec<String> = commands.iter().map(|path| EbiCommand2::path_to_string(path)).collect();
                writeln!(f, "{} {} can be used in:\n\t{}", object.get_type().get_article().to_string().to_sentence_case(), object.get_type(), paths.join("\n\t"))?;
            }
        } else {
            unreachable!()
        }
        
        Ok(EbiOutput::String(String::from_utf8(f).unwrap()))
    }, 
    output: &EbiOutputType::String
};

pub trait Infoable {
    fn info(&self, f: &mut impl std::io::Write) -> Result<()>;
}

impl Infoable for String {
    fn info(&self, f: &mut impl std::io::Write) -> Result<()> {
        Ok(writeln!(f, "Length\t{}", self.len())?)
    }
}

impl Infoable for BigFraction {
    fn info(&self, f: &mut impl std::io::Write) -> Result<()> {
        match self {
            fraction::GenericFraction::Rational(sign, ratio) => write!(f, "{} bits / {} bits", ratio.numer().bits(), ratio.denom().bits())?,
            fraction::GenericFraction::Infinity(sign) => write!(f, "{} infinity", sign.to_string())?,
            fraction::GenericFraction::NaN => write!(f, "NaN")?,
        }
        Ok(write!(f, "")?)
    }
}