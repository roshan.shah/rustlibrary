use std::{io::{self, IsTerminal}, path::PathBuf};
use anyhow::{anyhow, Context, Result};
use clap::{value_parser, Arg, ArgAction, ArgMatches, Command};

use crate::{ebi_input_output::{EbiInput, EbiInputType, EbiOutput, EbiOutputType}, ebi_objects::{directly_follows_model::DirectlyFollowsModel, ebi_object::{EbiObject, EbiObjectType, EbiTraitObject}, finite_stochastic_language::FiniteStochasticLanguage, labelled_petri_net::LabelledPetriNet, stochastic_deterministic_finite_automaton::StochasticDeterministicFiniteAutomaton}, ebi_traits::ebi_trait::EbiTrait, export, import::{self, MultipleReader}};
use super::{ebi_command::EbiCommand2, ebi_command_analyse::EBI_ANALYSE, ebi_command_discover::EBI_DISCOVER};

pub const EBI_CONVERT: EbiCommand2 = EbiCommand2::Group { 
    name_short: "convert", 
    explanation_short: "convert an object", 
    explanation_long: None, 
    children: &[ 
        &EBI_CONVERT_LPN,
        &EBI_CONVERT_SLANG,
        &EBI_CONVERT_SDFA
    ]
};

pub const EBI_CONVERT_LPN: EbiCommand2 = EbiCommand2::Command { 
    name_short: "lpn", 
    name_long: Some("labelled-Petri-net"),
    explanation_short: "Convert an object to a labelled Petri net.", 
    explanation_long: None, 
    latex_link: None, 
    cli_command: None, 
    exact_arithmetic: false, 
    input_types: &[ 
        &[
            &EbiInputType::ObjectType(EbiObjectType::DirectlyFollowsModel),
            &EbiInputType::ObjectType(EbiObjectType::StochasticLabelledPetriNet),
            &EbiInputType::ObjectType(EbiObjectType::LabelledPetriNet),
            &EbiInputType::ObjectType(EbiObjectType::StochasticDeterministicFiniteAutomaton),
            &EbiInputType::String
        ] ], 
    input_names: &[ "FILE" ], 
    input_helps: &[ "Any file supported by Ebi that can be converted." ], 
    execute: |mut inputs, cli_matches| {
        let lpn = match inputs.remove(0) {
            EbiInput::Object(EbiObject::DirectlyFollowsModel(dfm)) => Ok(dfm.to_labelled_petri_net()),
            EbiInput::Object(EbiObject::StochasticLabelledPetriNet(slpn)) => Ok(slpn.to_labelled_petri_net()),
            EbiInput::Object(EbiObject::LabelledPetriNet(lpn)) => Ok(lpn),
            EbiInput::Object(EbiObject::StochasticDeterministicFiniteAutomaton(sdfa)) => Ok(sdfa.to_stochastic_labelled_petri_net().to_labelled_petri_net()),
            EbiInput::String(s) => s.parse::<LabelledPetriNet>(),
            _ => unreachable!()
        }?;
        Ok(EbiOutput::Object(EbiObject::LabelledPetriNet(lpn)))
    }, 
    output: &EbiOutputType::ObjectType(EbiObjectType::LabelledPetriNet) 
};

pub const EBI_CONVERT_SLANG: EbiCommand2 = EbiCommand2::Command { 
    name_short: "slang", 
    name_long: Some("finite-stochastic-language"), 
    explanation_short: "Convert an object to a finite stochastic language.", 
    explanation_long: None, 
    latex_link: None, 
    cli_command: None, 
    exact_arithmetic: true, 
    input_types: &[ 
        &[
            &EbiInputType::ObjectType(EbiObjectType::FiniteStochasticLanguage),
            &EbiInputType::ObjectType(EbiObjectType::EventLog),
            &EbiInputType::String
        ] ], 
    input_names: &[ "FILE" ], 
    input_helps: &[ "Any file supported by Ebi that can be converted." ], 
    execute: |mut inputs, cli_matches| {
        let slang = match inputs.remove(0) {
            EbiInput::Object(EbiObject::FiniteStochasticLanguage(slang)) => Ok(slang),
            EbiInput::Object(EbiObject::EventLog(log)) => Ok(log.to_finite_stochastic_language()),
            EbiInput::String(s) => s.parse::<FiniteStochasticLanguage>(),
            _ => unreachable!()
        }?;
        Ok(EbiOutput::Object(EbiObject::FiniteStochasticLanguage(slang)))
    }, 
    output: &EbiOutputType::ObjectType(EbiObjectType::FiniteStochasticLanguage) 
};

pub const EBI_CONVERT_SDFA: EbiCommand2 = EbiCommand2::Command { 
    name_short: "sdfa", 
    name_long: Some("stochastic-finite-deterministic-automaton"), 
    explanation_short: "Convert an object to a finite stochastic language.", 
    explanation_long: None, 
    latex_link: None, 
    cli_command: None, 
    exact_arithmetic: true, 
    input_types: &[ 
        &[
            &EbiInputType::ObjectType(EbiObjectType::FiniteStochasticLanguage),
            &EbiInputType::ObjectType(EbiObjectType::StochasticDeterministicFiniteAutomaton),
            &EbiInputType::ObjectType(EbiObjectType::EventLog),
            &EbiInputType::String
        ] ], 
    input_names: &[ "FILE" ], 
    input_helps: &[ "Any file supported by Ebi that can be converted." ], 
    execute: |mut inputs, _| {
        let sdfa = match inputs.remove(0) {
            EbiInput::Object(EbiObject::FiniteStochasticLanguage(mut slang)) => Ok(slang.to_stochastic_deterministic_finite_automaton()),
            EbiInput::Object(EbiObject::StochasticDeterministicFiniteAutomaton(sdfa)) => Ok(sdfa),
            EbiInput::Object(EbiObject::EventLog(mut log)) => Ok(log.to_stochastic_deterministic_finite_automaton()),
            EbiInput::String(s) => s.parse::<StochasticDeterministicFiniteAutomaton>(),
            _ => unreachable!()
        }?;
        Ok(EbiOutput::Object(EbiObject::StochasticDeterministicFiniteAutomaton(sdfa)))
    }, 
    output: &EbiOutputType::ObjectType(EbiObjectType::StochasticDeterministicFiniteAutomaton)
};