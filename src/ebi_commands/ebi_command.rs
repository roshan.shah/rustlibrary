use std::{fmt::{Debug, Display}, hash::Hash, io::{self, IsTerminal}, path::PathBuf};

use clap::{value_parser, Arg, ArgAction, ArgMatches, Command};
use anyhow::{anyhow, Context, Result};
use quick_xml::name;

use crate::{ebi_commands::{ebi_command_analyse, ebi_command_association, ebi_command_conformance, ebi_command_convert, ebi_command_discover, ebi_command_latex, ebi_command_probability, ebi_command_validate, ebi_command_visualise}, ebi_input_output::{self, EbiInput, EbiInputType, EbiOutput, EbiOutputType}, ebi_objects::ebi_object::{EbiObject, EbiObjectType, EbiTraitObject}, ebi_traits::ebi_trait::EbiTrait, export::{self, Exportable}, import::{self, MultipleReader}, math::fraction::Fraction};

use super::ebi_command_info;

pub const EBI_COMMANDS: EbiCommand2 = EbiCommand2::Group {
    name_short: "Ebi",
    explanation_short: "Ebi: a tool for stochastic process mining.",
    explanation_long: None, 
    children: &[
        &ebi_command_analyse::EBI_ANALYSE,
        &ebi_command_association::EBI_ASSOCIATION,
        &ebi_command_conformance::EBI_CONFORMANCE,
        &ebi_command_convert::EBI_CONVERT,
        &ebi_command_discover::EBI_DISCOVER,
        &ebi_command_latex::EBI_LATEX,
        &ebi_command_info::EBI_INFO,
        &ebi_command_probability::EBI_PROBABILITY,
        &ebi_command_validate::EBI_VALIDATE,
        &ebi_command_visualise::EBI_VISUALISE,
    ] 
};

pub const ARG_SHORT_OUTPUT: char = 'o';
pub const ARG_SHORT_APPROX: char = 'a';
pub const ARG_ID_OUTPUT: &str = "output";

pub enum EbiCommand2 {
    Group{
        name_short: &'static str,
        explanation_short: &'static str,
        explanation_long: Option<&'static str>,
        children: &'static [&'static EbiCommand2]
    },
    Command{
        name_short: &'static str,
        name_long: Option<&'static str>,
        explanation_short: &'static str,
        explanation_long: Option<&'static str>,
        latex_link: Option<&'static str>, //insert a link to a Latex target
        cli_command: Option<fn(command: Command) -> Command>, //create the cli command. An output -o argument is always added

        exact_arithmetic: bool, //if set to true, then Ebi will use exact arithmetic by default, but allow the user to disable it
        
        input_types: &'static [&'static [&'static EbiInputType]], //for each fixed-position input parameter, the ebi traits that are accepted. If accept_source_file is true, that is the first parameter
        input_names: &'static [&'static str],
        input_helps: &'static [&'static str],

        execute: fn(inputs: Vec<EbiInput>, cli_matches: &ArgMatches) -> Result<EbiOutput>,
        output: &'static EbiOutputType
    }
}

impl EbiCommand2 {
    pub(crate) fn build_cli(&self) -> Command {
        let mut command;
        match self {
            EbiCommand2::Group { name_short, explanation_short, explanation_long, children } => {
                command = Command::new(name_short)
                    .about(explanation_short)
                    .subcommand_required(true)
                    .allow_external_subcommands(false);

                if let Some(l) = explanation_long {
                    command = command.long_about(l);
                }
                
                for child in children.iter() {
                    let subcommand = child.build_cli();
                    command = command.subcommand(subcommand);
                }
            },
            EbiCommand2::Command { name_short, name_long, explanation_short, explanation_long, cli_command, latex_link, exact_arithmetic, execute, input_types, input_helps: input_help, input_names, output } => {
                command = Command::new(name_short)
                    .about(explanation_short);

                if let Some(l) = explanation_long {
                    command = command.long_about(l);
                }

                for (i, (input_name, (input_type, input_help))) in input_names.iter().zip(input_types.iter().zip(input_help.iter())).enumerate() {
                    let mut arg = Arg::new(format!("{}x{}", input_name, i))
                    .action(ArgAction::Set)
                    .value_name(input_name)
                    .help(input_help)
                    .required(true) //TODO: handle stdin
                    .value_parser(EbiInputType::get_parser_of_list(input_type))
                    .long_help(EbiInputType::get_possible_inputs(input_type));

                    command = command.arg(arg);
                }
                // if *accept_source_file {
                //     command = command.arg(
                //         Arg::new("source")
                //         .action(ArgAction::Set)
                //         .value_name("SOURCE_FILE")
                //         .help("The first file to import (may be piped in)")
                //         .required(io::stdin().is_terminal())
                //         .value_parser(value_parser!(PathBuf))
                //     )
                // }

                if let Some(f) = cli_command {
                    command = (f)(command);   
                }

                command = command.arg(
                    Arg::new(ARG_ID_OUTPUT)
                    .short(ARG_SHORT_OUTPUT)
                    .long(ARG_ID_OUTPUT)
                    .action(ArgAction::Set)
                    .value_name("FILE")
                    .help("Saves the result to a file.")
                    .required(false)
                    .value_parser(value_parser!(PathBuf))
                );

                if *exact_arithmetic {
                    command = command.arg(
                        Arg::new("approx")
                        .short(ARG_SHORT_APPROX)
                        .long("approximate")
                        .action(ArgAction::SetTrue)
                        .num_args(0)
                        .help("Use approximate arithmetic instead of exact arithmetic.")
                        .required(false)
                        .value_parser(value_parser!(bool))
                    )
                }
            },
        };
        return command;
    }

    pub fn short_name(&self) -> &str {
        match self {
            EbiCommand2::Group { name_short, explanation_short, explanation_long, children } => name_short,
            EbiCommand2::Command { name_short, name_long, explanation_short, explanation_long, cli_command, latex_link, exact_arithmetic, input_types, execute, output, input_helps: input_help, input_names } => name_short,
        }
    }

    pub fn long_name(&self) -> &str {
        match self {
            EbiCommand2::Group { name_short, explanation_short, explanation_long, children } => self.short_name(),
            EbiCommand2::Command { name_short, name_long, explanation_short, explanation_long, latex_link, cli_command, exact_arithmetic, input_types: input_traits, input_names, input_helps, execute, output } => match name_long {
                Some(x) => x,
                None => &name_short,
            },
        }
    }

    pub fn execute(&self, cli_matches: &ArgMatches) -> Result<EbiOutput> {
        match self {
            EbiCommand2::Group { name_short, explanation_short, explanation_long, children } => {
                for child in children.iter() {
                    if let Some(sub_matches) = cli_matches.subcommand_matches(child.short_name()) {
                        return child.execute(sub_matches);
                    }
                }
            },
            EbiCommand2::Command { name_short, name_long, explanation_short, explanation_long, cli_command, latex_link, exact_arithmetic, input_types: input_typess, execute, output, input_helps: input_help, input_names } => {
                //set exact arithmetic
                if !exact_arithmetic || cli_matches.get_flag("approx") {
                    log::info!("Use approximate arithmetic");
                    Fraction::set_exact_globally(false);
                }

                //read the inputs
                let mut inputs = vec![];
                for (i, (input_types, input_name)) in input_typess.iter().zip(input_names.iter()).enumerate() {
                    let cli_id = format!("{}x{}", input_name, i);

                    //read input
                    log::info!("Reading {}", input_name);
                    if let Ok(input) = Self::attempt_parse(input_types, cli_matches, &cli_id) {
                        inputs.push(input);
                    } else {
                        return Err(anyhow!("Could not read parameter {}", input_name));
                    }
                }

                log::info!("Starting {}", self.long_name());

                let result = (execute)(inputs, cli_matches)?;

                if &&result.get_type() != output {
                    return Err(anyhow!("Output type {} does not match the declared output of {}.", result.get_type(), output))
                }

                if let Some(to_file) = cli_matches.get_one::<PathBuf>(ARG_ID_OUTPUT) {
                    //write result to file
                    log::info!("Writing result to {:?}", to_file);
                    export::export_object2(to_file, &result)?;
                } else {
                    //write result to STDOUT
                    println!("{}", export::export_to_string(&result)?);
                }

                return Ok(result);
            },
        }
        Err(anyhow!("command not recognised"))
    }

    /**
     * Attempt to parse an input as any of the given input types. Returns the last error if unsuccessful.
     */
    fn attempt_parse(input_types: &[&EbiInputType], cli_matches: &ArgMatches, cli_id: &str) -> Result<EbiInput> {
        //an input may be of several types; go through each of them
        let mut error = None;
        for input_type in input_types.iter() {
            //try to parse the input as this type
            match input_type {
                EbiInputType::Trait(etrait) => {
                    
                    //try to parse a trait
                    if let Some(to_file) = cli_matches.get_one::<PathBuf>(cli_id) {
                        match import::get_reader_file(to_file).context("Could not get reader for file.") {
                            Ok(mut reader) => {
                                match import::read_as_trait(etrait, &mut reader).context("Could not parse file as trait.") {
                                    Ok(object) => return Ok(EbiInput::Trait(object)),
                                    Err(e) => error = Some(e)
                                }
                            },
                            Err(e) => error = Some(e)
                        }
                    }
                },
                EbiInputType::ObjectType(etype) => {
                    
                    //try to parse a specific object
                    if let Some(to_file) = cli_matches.get_one::<PathBuf>(cli_id) {
                        match import::get_reader_file(to_file).context("Could not get reader for file.") {
                            Ok(mut reader) => {
                                match import::read_as_object(etype, &mut reader).context("Could not parse file as object.") {
                                    Ok(object) => return Ok(EbiInput::Object(object)),
                                    Err(e) => error = Some(e)
                                }
                            },
                            Err(e) => error = Some(e)
                        }
                    }

                },
                EbiInputType::AnyObject => {
                    if let Some(to_file) = cli_matches.get_one::<PathBuf>(cli_id) {
                        match import::get_reader_file(to_file).context("Could not get reader for file.") {
                            Ok(mut reader) => {
                                match import::read_as_any_object(&mut reader).context("Could not parse file as object.") {
                                    Ok(object) => return Ok(EbiInput::Object(object)),
                                    Err(e) => error = Some(e)
                                }
                            },
                            Err(e) => error = Some(e)
                        }
                    }
                },
                EbiInputType::FileHandler => todo!(),
                EbiInputType::String => {
                    if let Some(value) = cli_matches.get_one::<String>(&cli_id) {
                        return Ok(EbiInput::String(value.clone()));
                    }
                },
                EbiInputType::Usize => {
                    if let Some(value) = cli_matches.get_one::<usize>(&cli_id) {
                        return Ok(EbiInput::Usize(value.clone()));
                    }
                },
                EbiInputType::Fraction => {
                    if let Some(value) = cli_matches.get_one::<Fraction>(&cli_id) {
                        return Ok(EbiInput::Fraction(value.clone()));
                    }
                },
            }
        }

        match error {
            Some(e) => Err(e),
            None => Err(anyhow!("argument was not given")),
        }
    } 

    pub fn path_to_string(path: &Vec<&EbiCommand2>) -> String {
        let result: Vec<&str> = path.iter().map(|command| command.long_name()).collect();
        result.join(" ")
    }
}

impl Display for EbiCommand2 {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.long_name())
    }
}

impl Debug for EbiCommand2 {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Group { name_short, explanation_short, explanation_long, children } => f.debug_struct("Group").field("name_short", name_short).finish(),
            Self::Command { name_short, name_long, explanation_short, explanation_long, latex_link, cli_command, exact_arithmetic, input_types: input_traits, input_names, input_helps, execute, output } => f.debug_struct("Command").field("name_short", name_short).field("name_long", name_long).finish(),
        }
    }
}

impl Eq for EbiCommand2 {}

impl PartialEq for EbiCommand2 {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Group { name_short: l_name_short, explanation_short: l_explanation_short, explanation_long: l_explanation_long, children: l_children }, Self::Group { name_short: r_name_short, explanation_short: r_explanation_short, explanation_long: r_explanation_long, children: r_children }) => l_name_short == r_name_short && l_explanation_short == r_explanation_short && l_explanation_long == r_explanation_long && l_children == r_children,
            (Self::Command { name_short: l_name_short, name_long: l_name_long, explanation_short: l_explanation_short, explanation_long: l_explanation_long, latex_link: l_latex_link, cli_command: l_cli_command, exact_arithmetic: l_exact_arithmetic, input_types: l_input_types, input_names: l_input_names, input_helps: l_input_helps, execute: l_execute, output: l_output }, Self::Command { name_short: r_name_short, name_long: r_name_long, explanation_short: r_explanation_short, explanation_long: r_explanation_long, latex_link: r_latex_link, cli_command: r_cli_command, exact_arithmetic: r_exact_arithmetic, input_types: r_input_types, input_names: r_input_names, input_helps: r_input_helps, execute: r_execute, output: r_output }) => l_name_short == r_name_short && l_name_long == r_name_long && l_explanation_short == r_explanation_short && l_explanation_long == r_explanation_long && l_latex_link == r_latex_link && l_cli_command == r_cli_command && l_exact_arithmetic == r_exact_arithmetic && l_input_types == r_input_types && l_input_names == r_input_names && l_input_helps == r_input_helps && l_execute == r_execute && l_output == r_output,
            _ => false,
        }
    }
}

impl Hash for EbiCommand2 {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.long_name().hash(state)
    }
}