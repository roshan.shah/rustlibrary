use std::{collections::HashSet, io::{self, Write}};

use clap::{ArgMatches, Command};
use anyhow::Result;
use fraction::Zero;
use crate::{ebi_input_output::{EbiInputType, EbiOutput, EbiOutputType}, ebi_objects::ebi_object::{EbiObject, EbiObjectType}, ebi_traits::ebi_trait::EbiTrait, file_handler::{EbiFileHandler, EBI_FILE_HANDLERS}, EBI_COMMANDS};

use super::ebi_command::EbiCommand2;

pub const EBI_LATEX: EbiCommand2 = EbiCommand2::Command { 
    name_short: "latex", 
    name_long: Some("help-latex"), 
    explanation_short: "Print the help of Ebi in Latex format.", 
    explanation_long: None, 
    cli_command: None, 
    latex_link: None, 
    exact_arithmetic: false, 
    input_types: &[], 
    input_names: &[],
    input_helps: &[],
    execute: |_, _| Ok(print_help2()?), 
    output: &EbiOutputType::String
};

fn print_help2() -> Result<EbiOutput> {
    let mut f = vec![];

    writeln!(f, "\\section{{Functions}}");
    print_inputs(&mut f);

    writeln!(f, "\\section{{Supported File Types}}");
    print_file_types(&mut f);
    Ok(EbiOutput::String(String::from_utf8(f).unwrap()))
}

fn print_file_types(f: &mut Vec<u8>) {
    writeln!(f, "\\begin{{tabularx}}{{\\linewidth}}{{XlX}}\\toprule");
    writeln!(f, "Type & extension & import as\\\\\\midrule");
    for file_type in EBI_FILE_HANDLERS {
        writeln!(f, "{}&", file_type.name);
        writeln!(f, ".{}&", file_type.file_extension);
        writeln!(f, "{}", import_as(file_type).join(", "));
        writeln!(f, "\\\\");
    }
    writeln!(f, "\\bottomrule\\end{{tabularx}}");
}

fn import_as(file_type: &EbiFileHandler) -> Vec<String> {
    file_type.trait_importers.iter().map(|f| f.to_string()).collect()
}

fn print_help_recursive(f: &mut Vec<u8>, command: &Command, depth: usize) {
    write!(f, "{}", "\\quad".repeat(depth));
    write!(f, "\\texttt{{{}}}&", command.get_name());
    write!(f, "{}", command.get_about().unwrap().to_string());
    if let Some(x) = command.get_author() {
        write!(f, " {}", x);
    }
    writeln!(f, "\\\\");
    for subcommand in command.get_subcommands() {
        print_help_recursive(f, subcommand, depth+1);
    }
}

fn print_inputs(f: &mut Vec<u8>) {
    print_inputs_recursive(f, &EBI_COMMANDS, vec![]);
}

fn print_inputs_recursive(f: &mut Vec<u8>, command: &EbiCommand2, super_commands: Vec<&str>) {
    match command {
        EbiCommand2::Group { name_short, explanation_short, explanation_long, children } => {
            if super_commands.len() == 0 {
                
            } else {
                if super_commands.len() == 1 {
                    writeln!(f, "\\subsection{{\\texttt{{{}}}}}", name_short);
                } else if super_commands.len() == 2 {
                    writeln!(f, "\\subsubsection{{\\texttt{{{}}}}}", name_short);
                } else if super_commands.len() == 3 {
                    writeln!(f, "\\paragraph{{\\texttt{{{}}}}}", name_short);
                }

                if let Some(l) = explanation_long {
                    writeln!(f, "{}", l);
                } else {
                    writeln!(f, "{}", explanation_short);
                }
            }

            for child in children.iter() {
                let mut s = super_commands.clone();
                s.push(name_short);
                print_inputs_recursive(f, child, s);
            }
        },
        EbiCommand2::Command { name_short, name_long, explanation_short, explanation_long, latex_link, cli_command, exact_arithmetic, input_types, input_names, input_helps: input_help, execute, output } => {
            let name = if let Some(x) = name_long {x} else {name_short};
            let full_path = super_commands.join(" ") + " " + name;
            
            if super_commands.len() == 1 {
                writeln!(f, "\\subsection{{\\texttt{{{}}}}}", name);
            } else if super_commands.len() == 2 {
                writeln!(f, "\\subsubsection{{\\texttt{{{}}}}}", name);
            } else if super_commands.len() == 3 {
                writeln!(f, "\\paragraph{{\\texttt{{{}}}}}", name);
            }

            if let Some(x) = name_long {
                writeln!(f, "Alias: \\texttt{{{}}}.\\\\", name_short);
            }

            writeln!(f, "Full path: \\texttt{{{}}}.\\\\", full_path);

            if let Some(l) = explanation_long {
                writeln!(f, "{}", l);
            } else {
                writeln!(f, "{}", explanation_short);
            }

            writeln!(f, "\\paragraph{{Parameters}}");
            writeln!(f, "~\\\\");
            writeln!(f, "\\begin{{tabularx}}{{\\linewidth}}{{lX}}");
            writeln!(f, "\\toprule");
            writeln!(f, "Parameter \\\\\\midrule");
            let mut pos = 0;

            //trait parameters
            for (i, (input_name, (input_trait, input_help))) in input_names.iter().zip(input_types.iter().zip(input_help.iter())).enumerate() {
                write!(f, "<\\texttt{{{}}}>", input_name.replace("_", "\\_"));    
                writeln!(f, "&{}\\\\", input_help);

                //mandatoryness
                writeln!(f, "&\\textit{{Mandatory:}} \\quad yes\\\\");

                //accepted traits
                writeln!(f, "&\\textit{{Accepted traits:}}\\quad {}\\\\", get_traits(input_trait));

                //accepted file types
                writeln!(f, "&\\textit{{Accepted file types:}}\\quad {}\\\\", get_file_types(input_trait));
            }

            //custom parameters
            if let Some(fu) = cli_command {
                for arg in (fu)(Command::new("")).get_arguments() {
                    if let Some(short) = arg.get_short() {
                        if let Some(long) = arg.get_long() {
                            writeln!(f, "-\\texttt{{{}}} or --\\texttt{{{}}}", short, long);
                        } else {
                            writeln!(f, "-\\texttt{{{}}}", short);
                        }
                    } else {
                        if let Some(long) = arg.get_long() {
                            writeln!(f, "--\\texttt{{{}}}", long);
                        } else {
                            writeln!(f, "<\\texttt{{{}}}>", arg.get_value_names().unwrap()[0]);
                        }
                    }
                    writeln!(f, "&{}\\\\", if let Some(h) = arg.get_long_help() {h.to_string()} else {arg.get_help().unwrap().to_string()});

                    if arg.get_short().is_none() && arg.get_long().is_none() {
                        writeln!(f, "&\\textit{{Mandatory:}}\\quad {}\\\\", if arg.is_required_set() {"yes"} else {"no"} );
                        // writeln!(f, "&\\textit{{Accepted file types:}}\\quad {}\\\\", get_file_types(input_types, pos));
                        writeln!(f, "&\\textit{{Accepted traits:}}\\quad {}\\\\", get_traits(input_types[pos]));

                        pos += 1;
                    } else {
                        writeln!(f, "&\\textit{{Mandatory:}}\\quad no\\\\");
                    }
                }
            }

            writeln!(f, "\\texttt{{-o}} or \\texttt{{--output}} <\\texttt{{FILE}}> & The file to which the result must be written. If not given, the results will be written to STDOUT.\\\\");
            writeln!(f, "&\\textit{{Mandatory:}} \\quad no\\\\");
            
            if *exact_arithmetic {
                writeln!(f, "\\texttt{{-a}} or \\texttt{{--approximate}} & Use approximate arithmetic instead of exact arithmetic.\\\\");
                writeln!(f, "&\\textit{{Mandatory:}}\\quad no\\\\");
            }

            writeln!(f, "\\bottomrule");
            writeln!(f, "\\end{{tabularx}}");

            writeln!(f, "");

            writeln!(f, "\\paragraph{{Output}}");
            writeln!(f, "~\\\\ {} (.{})", output, output.get_suggested_extension());
        },
    }

    fn get_file_types(input_types: &'static [&'static EbiInputType]) -> String {
        // let mut result = HashSet::new();
        // for x in input_types.iter() {
        //     result.extend(x.get_file_handlers());
        // }

        // let mut ft: Vec<String> = result.iter().map(|x| x.to_string() + " (" + x.file_extension + ")").collect();
        // ft.sort();
        // if ft.is_empty() {
        //     "n/a".to_string()
        // } else {
        //     ft.join(", ")
        // }
        todo!()
    }

    fn get_traits(input_types: &'static [&'static EbiInputType]) -> String {
        let mut result: Vec<String> = input_types.iter().map(|x| x.to_string()).collect();
        result.sort();
        result.join(", ")
    } 
}