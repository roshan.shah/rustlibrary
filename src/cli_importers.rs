use core::fmt;
use std::{io::{BufReader, self, BufWriter, Write, IsTerminal}, path::PathBuf, fs::File};

use clap::{ArgMatches, Command, value_parser, arg};
use anyhow::{Result, Context, anyhow};

use crate::{stochastic_labelled_petri_net::StochasticLabelledPetriNet, stochastic_deterministic_finite_automaton::StochasticDeterministicFiniteAutomaton,import_reader::{ImportReader, ImportRead, ImportReaderResettable}, labelled_petri_net::LabelledPetriNet, EbiObject, stochastic_language::{StochasticLanguage, StochasticLanguageStream,}};

pub trait Importable {
    fn from_reader(reader: &mut dyn ImportRead) -> Result<Self> where Self: Sized;
}

pub enum AnyLogModel {
    StochasticLanguage(StochasticLanguage),
    StochasticLabelledPetriNet(StochasticLabelledPetriNet),
    StochasticDeterministicFiniteAutomaton(StochasticDeterministicFiniteAutomaton)
}

pub enum AnyLogModelStream<'a> {
    StochasticLanguageStream(StochasticLanguageStream<'a>),
    StochasticLabelledPetriNet(StochasticLabelledPetriNet),
    StochasticDeterministicFiniteAutomaton(StochasticDeterministicFiniteAutomaton)
}


pub fn validate<X: Importable> (reader: &mut dyn ImportRead) -> Result<()> {
    match X::from_reader(reader) {
        Ok(_) => Ok(()),
        Err(x) => Err(x),
    }
}

pub fn cli_input_slpn_stdin(command: Command) -> Command {
    command.arg(
        arg!(<SLPN_FILE> "file path to the stochastic labelled Petri net (may be omitted if piped in)")
            .required(io::stdin().is_terminal())
            .value_parser(value_parser!(PathBuf)),
    )
}

pub fn cli_input_sdfa_stdin(command: Command) -> Command {
    command.arg(
        arg!(<SDFA_FILE> "file path to the stochastic labelled Petri net (may be omitted if piped in)")
            .required(io::stdin().is_terminal())
            .value_parser(value_parser!(PathBuf)),
    )
}

pub fn cli_input_any_stdin(command: Command) -> Command {
    command.arg(
        arg!(<FILE> "file path (may be omitted if piped in)")
            .required(io::stdin().is_terminal())
            .value_parser(value_parser!(PathBuf)),
    )
}

pub fn get_reader(matches: &ArgMatches, option: &str) -> Result<Box<dyn ImportRead>> {
    if let Some(from_file) = matches.get_one::<PathBuf>(option) {
        log::info!("read from file {:?}", from_file);
        let result = ImportReader::<BufReader<File>>::from_path(from_file).with_context(|| format!("could not read file `{}`", from_file.display()))?;
        return Ok(Box::new(result));
    } else {
        log::info!("read from stdin");
        let stdin = io::stdin();
        let reader = stdin.lock();
        let reader2 = ImportReader::new(reader);
        return Ok(Box::new(reader2));
    }
}

pub fn get_reader_resettable(matches: &ArgMatches, option: &str) -> Result<ImportReaderResettable> {
    if let Some(from_file) = matches.get_one::<PathBuf>(option) {
        log::info!("read from file {:?}", from_file);
        let result = ImportReaderResettable::from_path(from_file).with_context(|| format!("could not read file `{}`", from_file.display()))?;
        return Ok(result);
    } else {
        log::info!("read from stdin");
        let stdin = io::stdin();
        let reader = stdin.lock();
        let reader2 = ImportReaderResettable::new(reader).context("could not read from stdin")?;
        return Ok(reader2);
    }
}

pub fn import_object(matches: &ArgMatches, option: &str) -> Result<EbiObject> {
    let mut reader = get_reader_resettable(matches, option)?;
    
    log::trace!("try to read as lpn");
    if let Ok(lpn) = LabelledPetriNet::from_reader(&mut reader).context("this is not a valid lpn") {
        return Ok(EbiObject::LabelledPetriNet(lpn));
    }

    reader.reset();
    log::trace!("try to read as slpn");
    if let Ok(slpn) = StochasticLabelledPetriNet::from_reader(&mut reader).context("this is not a valid slpn") {
        return Ok(EbiObject::StochasticLabelledPetriNet(slpn));
    }

    reader.reset();
    log::trace!("try to read as stochastic language");
    if let Ok(lang) = StochasticLanguage::from_reader(&mut reader).context("this is not a valid stochastic language") {
        return Ok(EbiObject::StochasticLanguage(lang));
    }

    reader.reset();
    log::trace!("try to read as sdfa");
    if let Ok(sdfa) = StochasticLanguage::from_reader(&mut reader).context("this is not a valid stochastic sdfa") {
        return Ok(EbiObject::StochasticDeterministicFiniteAutomaton(sdfa));
    }

    Err(anyhow!("file not recognised"))
}

pub fn import_lpn(matches: &ArgMatches, option: &str) -> Result<LabelledPetriNet> {
    if let Some(from_file) = matches.get_one::<PathBuf>(option) {
        log::info!("read LPN from file {:?}", from_file);

        let mut reader = ImportReader::<BufReader<File>>::from_path(from_file).with_context(|| format!("could not read file `{}`", from_file.display()))?;
        return LabelledPetriNet::from_reader(&mut reader).with_context(|| format!("file `{}` is not a valid slpn file", from_file.display()));
    } else {
        log::info!("read LPN from stdin");
        
        let mut reader = ImportReader::new(BufReader::new(io::stdin().lock()));
        return LabelledPetriNet::from_reader(&mut reader).context("provided slpn is not valid");
    }
}

pub fn import_slpn(matches: &ArgMatches, option: &str) -> Result<StochasticLabelledPetriNet> {
    if let Some(from_file) = matches.get_one::<PathBuf>(option) {
        log::info!("read SLPN from file {:?}", from_file);

        let mut reader = ImportReader::<BufReader<File>>::from_path(from_file).with_context(|| format!("could not read file `{}`", from_file.display()))?;
        return StochasticLabelledPetriNet::from_reader(&mut reader).with_context(|| format!("file `{}` is not a valid slpn file", from_file.display()));
    } else {
        log::info!("read SLPN from stdin");
        
        let mut reader = ImportReader::new(BufReader::new(io::stdin().lock()));
        return StochasticLabelledPetriNet::from_reader(&mut reader).context("provided slpn is not valid");
    }
}

pub fn import_sdfa(matches: &ArgMatches, option: &str) -> Result<StochasticDeterministicFiniteAutomaton> {
    if let Some(from_file) = matches.get_one::<PathBuf>(option) {
        log::info!("read sdfa from file {:?}", from_file);

        let mut reader = ImportReader::<BufReader<File>>::from_path(from_file).with_context(|| format!("could not read file `{}`", from_file.display()))?;
        return StochasticDeterministicFiniteAutomaton::from_reader(&mut reader).with_context(|| format!("file `{}` is not a valid sdfa file", from_file.display()));
    } else {
        log::info!("read sdfa from stdin");
        
        let mut reader = ImportReader::new(BufReader::new(io::stdin().lock()));
        return StochasticDeterministicFiniteAutomaton::from_reader(&mut reader).context("provided sdfa is not valid");
    }
}

pub fn export_object(matches: &ArgMatches, option: &str, object: impl fmt::Display) -> Result<()> {
    if let Some(to_file) = matches.get_one::<PathBuf>(option) {
        log::info!("write object to {:?}", to_file);
        
        let file = File::create(to_file).with_context(|| format!("writing {} to file {:?}", option, to_file))?;
        let mut writer = BufWriter::new(&file);
        write!(writer, "{}", object).with_context(|| format!("writing {} to file {:?}", option, to_file))?;
        return writer.flush().with_context(|| format!("writing {} to file {:?}", option, to_file));
    } else {
        print!("{}", object);
        return Ok(());
    }
}

pub fn validate_object_of(matches: &ArgMatches, option: &str, file_handler: &crate::EbiFileHandler) -> Result<()> {
    let mut binding = get_reader(matches, option).context("could not read object")?;
    let reader = binding.as_mut();
    let result = (file_handler.validator)(reader);
    return result;
}

pub fn import_log(reader: &mut dyn ImportRead) -> Result<StochasticLanguage> {
    log::trace!("try to read as stochastic language");
    let result = StochasticLanguage::from_reader(reader).context("parse stochastic language")?;

    return Ok(result);
}

pub fn import_log_stream (reader: &mut dyn ImportRead) -> Result<StochasticLanguageStream> {
    log::trace!("try to read as stochastic language");
    let result = StochasticLanguageStream::from_reader(reader).context("parse stochastic language")?;

    return Ok(result);
}

pub fn import_log_or_model(reader: &mut dyn ImportRead) -> Result<AnyLogModel> {
    if let Ok(slpn) = StochasticLabelledPetriNet::from_reader(reader).context("parse stochastic labelled Petri net") {
        return Ok(AnyLogModel::StochasticLabelledPetriNet(slpn));
    }

    if let Ok(lang) = StochasticLanguage::from_reader(reader).context("parse stochastic language") {
        return Ok(AnyLogModel::StochasticLanguage(lang));
    }

    if let Ok(sdfa) = StochasticDeterministicFiniteAutomaton::from_reader(reader).context("parse stochastic dfa") {
        return Ok(AnyLogModel::StochasticDeterministicFiniteAutomaton(sdfa));
    }

    return Err(anyhow!("File not recognised. To see what is wrong, try to parse it using `ebi validate`."));
}

pub fn import_log_or_model_stream(reader: &mut dyn ImportRead) -> Result<AnyLogModelStream> {
    if let Ok(slpn) = StochasticLabelledPetriNet::from_reader(reader).context("parse stochastic labelled Petri net") {
        return Ok(AnyLogModelStream::StochasticLabelledPetriNet(slpn));
    }

    if let Ok(lang) = StochasticLanguageStream::from_reader(reader).context("parse stochastic language") {
        return Ok(AnyLogModelStream::StochasticLanguageStream(lang));
    }

    if let Ok(sdfa) = StochasticDeterministicFiniteAutomaton::from_reader(reader).context("parse sdfa") {
        return Ok(AnyLogModelStream::StochasticDeterministicFiniteAutomaton(sdfa));
    }


    return Err(anyhow!("File not recognised. To see what is wrong, try to parse it using `ebi validate`."));
}