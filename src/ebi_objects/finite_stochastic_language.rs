use std::{collections::{hash_map::Iter, HashMap, HashSet}, fmt, io::{self, BufRead, Cursor}, rc::Rc, str::FromStr};

use anyhow::{anyhow, Context, Error, Result};
use fraction::Zero;
use num_traits::One;
use crate::{ebi_commands::ebi_command_info::Infoable, ebi_traits::{ebi_trait_finite_language::EbiTraitFiniteLanguage, ebi_trait_finite_stochastic_language::EbiTraitFiniteStochasticLanguage, ebi_trait_iterable_stochastic_language::{EbiTraitIterableStochasticLanguage, EbiTraitStochasticLanguageIterator}, ebi_trait_queriable_stochastic_language::EbiTraitQueriableStochasticLanguage, ebi_trait_stochastic_semantics::{EbiTraitStochasticSemantics, StochasticSemantics, ToStochasticSemantics}}, export::Exportable, file_handler::EbiFileHandler, follower_semantics::FollowerSemantics, import::{self, EbiObjectImporter, EbiTraitImporter, Importable}, line_reader::LineReader, math::fraction::Fraction, Trace};

use super::{ebi_object::EbiObject, finite_language::FiniteLanguage, finite_stochastic_language_semantics::FiniteStochasticLanguageSemantics, stochastic_deterministic_finite_automaton::StochasticDeterministicFiniteAutomaton};

pub const HEADER: &str = "finite stochastic language";

pub const EBI_FINITE_STOCHASTIC_LANGUAGE: EbiFileHandler = EbiFileHandler {
    name: "finite stochastic language",
    article: "a",
    file_extension: "slang",
    validator: import::validate::<FiniteStochasticLanguage>,
    trait_importers: &[
        EbiTraitImporter::FiniteLanguage(FiniteStochasticLanguage::read_as_finite_language),
        EbiTraitImporter::FiniteStochasticLanguage(import::read_as_finite_stochastic_language::<FiniteStochasticLanguage>),
        EbiTraitImporter::QueriableStochasticLanguage(import::read_as_queriable_stochastic_language::<FiniteStochasticLanguage>),
        EbiTraitImporter::IterableStochasticLanguage(import::read_as_iterable_stochastic_language::<FiniteStochasticLanguage>),
        EbiTraitImporter::StochasticSemantics(FiniteStochasticLanguage::import_as_stochastic_semantics),
    ],
    object_importers: &[
        EbiObjectImporter::FiniteStochasticLanguage(FiniteStochasticLanguage::import_as_object),
    ]
};

pub struct FiniteStochasticLanguage {
    traces: HashMap<Trace, Fraction>
}

impl FiniteStochasticLanguage {

    /**
     * Normalises the distribution. Use new_raw to avoid normalisation.
     */
    pub fn new(mut traces: HashMap<Vec<String>, Fraction>) -> Self {
        Self::normalise(&mut traces);
        Self {
            traces,
        }
    }

    /**
     * Does not normalise the distribution.
     */
    pub fn new_raw(traces: HashMap<Vec<String>, Fraction>) -> Self {
        Self {
            traces: traces
        }
    }

    pub fn read_as_finite_language(reader: &mut dyn BufRead) -> Result<Box<dyn EbiTraitFiniteLanguage>> {
        let lang = Self::import(reader)?;
        Ok(Box::new(lang.to_finite_language()))
    }

    pub fn normalise(traces: &mut HashMap<Vec<String>, Fraction>) {
        let sum = traces.values().fold(Fraction::zero(), |x, y| &x + y);
        log::info!("the extracted traces cover a sum of {}", sum);
        traces.retain(|_, v| {*v /= &sum;true});
    }

    pub fn to_stochastic_deterministic_finite_automaton(&mut self) -> StochasticDeterministicFiniteAutomaton {
        log::info!("convert finite stochastic language to SDFA");

        let mut result = StochasticDeterministicFiniteAutomaton::new();
        let mut final_states = HashMap::new();

        //create automaton
        for (trace, probability) in &self.traces {
            let trace = result.get_activity_key().process_trace(&trace);

            let mut state = result.get_initial_state();
            for activity in trace {
                state = result.take_or_add_transition(state, activity, probability.clone());
            }

            match final_states.entry(state) {
                std::collections::hash_map::Entry::Occupied(mut e) => {*e.get_mut() += Fraction::one()},
                std::collections::hash_map::Entry::Vacant(e) => {e.insert(Fraction::one());},
            }
        }

        //count
        let mut sums = final_states;
        for (source, _, _, probability) in &result {
            match sums.entry(*source) {
                std::collections::hash_map::Entry::Occupied(mut e) => {*e.get_mut() += probability},
                std::collections::hash_map::Entry::Vacant(e) => {e.insert(probability.clone());},
            }
        }

        //normalise
        for (source, _, _, probability) in &mut result {
            *probability /= sums.get(source).unwrap();
        }

        result
    }

    pub fn to_finite_language(self) -> FiniteLanguage {
        log::info!("create finite language");

        let mut map: HashSet<Trace> = HashSet::new();
        for (trace, _) in self.traces {
            map.insert(trace);
        }

        FiniteLanguage::from(map)
    }
}

impl EbiTraitFiniteStochasticLanguage for FiniteStochasticLanguage {
    fn len(&self) -> usize {
        self.traces.len()
    }
    
    fn get(&self, trace_index: usize) -> Option<(&Trace, &Fraction)> {
        self.traces.iter().nth(trace_index)
    }
}

impl FromStr for FiniteStochasticLanguage {
    type Err = Error;

    fn from_str(s: &str) -> std::prelude::v1::Result<Self, Self::Err> {
        let mut reader = io::Cursor::new(s);
        Self::import(&mut reader)
    }
}

impl Importable for FiniteStochasticLanguage {
    fn import_as_object(reader: &mut dyn BufRead) -> Result<EbiObject> {
        Ok(EbiObject::FiniteStochasticLanguage(Self::import(reader)?))
    }

    fn import(reader: &mut dyn BufRead) -> Result<Self> {
        let mut lreader = LineReader::new(reader);

        let head = lreader.next_line_string().with_context(|| format!("failed to read header, which should be `{}`", HEADER))?;
        if head != HEADER {
            return Err(anyhow!("first line should be exactly `{}`, but found `{}`", HEADER, head));
        }

        let number_of_traces = lreader.next_line_index().context("failed to read number of places")?;
        if number_of_traces == 0 {
            return Err(anyhow!("language is empty"));
        }

        let mut traces = HashMap::new();
        let mut sum = Fraction::zero();
        for trace_i in 0 .. number_of_traces {
            let probability = lreader.next_line_weight().with_context(|| format!("failed to read weight for trace {} at line {}", trace_i, lreader.get_last_line_number()))?;

            if !probability.is_positive() {
                return Err(anyhow!("trace {} at line {} has non-positive probability", trace_i, lreader.get_last_line_number()));
            } else if probability > Fraction::one() {
                return Err(anyhow!("trace {} at line {} has a probability higher than 1", trace_i, lreader.get_last_line_number()));
            }

            sum += probability.clone();

            let number_of_events = lreader.next_line_index().with_context(|| format!("failed to read number of events for trace {} at line {}", trace_i, lreader.get_last_line_number()))?;

            let mut trace = vec![];
            trace.reserve_exact(number_of_events);

            for event_i in 0 .. number_of_events {
                let event = lreader.next_line_string().with_context(|| format!("failed to read event {} of trace {} at line {}", event_i, trace_i, lreader.get_last_line_number()))?;
                trace.push(event);
            }

            if traces.insert(trace, probability).is_some() {
                return Err(anyhow!("trace {} ending at line {} appears twice in language", trace_i, lreader.get_last_line_number()));    
            }
        }

        if Fraction::is_exaxt_globally() && !sum.is_one() {
            return Err(anyhow!("probabilities in language do not sum to 1, but to {}", sum));
        }

        Ok(Self {
            traces: traces
        })
    }
}

impl Exportable for FiniteStochasticLanguage {
    fn export(&self, f: &mut impl std::io::Write) -> Result<()> {
        Ok(write!(f, "{}", self)?)
    }
}

impl Infoable for FiniteStochasticLanguage {
    fn info(&self, f: &mut impl std::io::Write) -> Result<()> {
        writeln!(f, "Number of traces\t{}", self.traces.len())?;
        writeln!(f, "Number of events\t{}", self.traces.iter().map(|t| t.0.len()).sum::<usize>())?;

        Ok(write!(f, "")?)
    }
}

impl fmt::Display for FiniteStochasticLanguage {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "{}", HEADER)?;
        writeln!(f, "# number of traces\n{}", self.traces.len())?;

        for (pos, (trace, probability)) in self.traces.iter().enumerate() {
            writeln!(f, "# trace {}", pos)?;

            writeln!(f, "# probability\n{}", probability)?;

            writeln!(f, "# number of events\n{}", trace.len())?;
            for event in trace {
                writeln!(f, "{}", event)?;
            }
        }

        write!(f, "")
    }
}

impl ToStochasticSemantics for FiniteStochasticLanguage {
    type State = <FiniteStochasticLanguageSemantics as StochasticSemantics>::State;

    fn get_stochastic_semantics(net: Rc<Self>) -> Box<dyn StochasticSemantics<State = Self::State>> {
        Box::new(FiniteStochasticLanguageSemantics::from_language(net))
    }

    fn import_as_stochastic_semantics(reader: &mut dyn BufRead) -> Result<EbiTraitStochasticSemantics> {
        let slang = Self::import(reader)?;
        let s = Rc::new(slang);
        Ok(EbiTraitStochasticSemantics::Usize(Self::get_stochastic_semantics(s)))
    }
}

impl EbiTraitIterableStochasticLanguage for FiniteStochasticLanguage {
    fn iter(&self) -> Box<dyn EbiTraitStochasticLanguageIterator + '_> {
        Box::new(FiniteStochasticLanguageIterator {
            iter: self.traces.iter()
        })
    }
}

pub struct FiniteStochasticLanguageIterator<'a> {
    iter: Iter<'a, Trace, Fraction>
}

impl<'a> EbiTraitStochasticLanguageIterator<'a> for FiniteStochasticLanguageIterator<'a> {}

impl <'a> Iterator for FiniteStochasticLanguageIterator<'a> {
    type Item = (&'a Trace, &'a Fraction);

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}

impl EbiTraitQueriableStochasticLanguage for FiniteStochasticLanguage {
    fn get_probability(&mut self, follower: &FollowerSemantics) -> Result<Fraction> {
        match follower {
            FollowerSemantics::Trace(trace) => {
                return match self.traces.get(*trace) {
                    Some(x) => Ok(x.clone()),
                    None => Ok(Fraction::zero()),
                };
            },
        }
    }

    fn get_sample(&mut self, number_of_traces: u64) -> Result<FiniteStochasticLanguage> {
        let mut result = HashMap::new();
        let mut sum = Fraction::zero();
        for (trace, probability) in self.traces.iter() {
            sum += probability.clone();
            if sum >= Fraction::from(number_of_traces) {
                result.insert(trace.clone(), probability.clone());
                break;
            }
        }

        Ok(Self::new_raw(result))
    }
   
}