use std::{cmp::{max, Ordering}, collections::{HashMap, HashSet}, fmt, io::{self, BufRead}, mem::zeroed, rc::Rc, slice::Iter, str::FromStr};
use anyhow::{anyhow, Context, Result, Error};
use num_traits::zero;
use rand::{thread_rng,Rng};
use fraction::{BigUint, GenericFraction, One, Zero};
use layout::topo::layout::VisualGraph;
use serde_json::Value;
use crate::{activity_key::{self, Activity, ActivityKey}, dottable::Dottable, ebi_commands::ebi_command_info::Infoable, ebi_traits::{ebi_trait_queriable_stochastic_language::EbiTraitQueriableStochasticLanguage, ebi_trait_stochastic_deterministic_semantics::{EbiTraitStochasticDeterministicSemantics, StochasticDeterministicSemantics}}, export::Exportable, file_handler::EbiFileHandler, follower_semantics::FollowerSemantics, import::{self, EbiObjectImporter, EbiTraitImporter, Importable}, marking::Marking, math::fraction::Fraction, net::Transition, Trace};

use super::{ebi_object::EbiObject, finite_stochastic_language::FiniteStochasticLanguage, labelled_petri_net::LabelledPetriNet, stochastic_labelled_petri_net::StochasticLabelledPetriNet};

pub const EBI_STOCHASTIC_DETERMINISTIC_FINITE_AUTOMATON: EbiFileHandler = EbiFileHandler {
    name: "stochastic deterministic finite automaton",
    article: "an",
    file_extension: "sdfa",
    validator: import::validate::<StochasticDeterministicFiniteAutomaton>,
    trait_importers: &[
        EbiTraitImporter::QueriableStochasticLanguage(import::read_as_queriable_stochastic_language::<StochasticDeterministicFiniteAutomaton>),
        EbiTraitImporter::StochasticDeterministicSemantics(StochasticDeterministicFiniteAutomaton::import_as_p_semantics),
    ],
    object_importers: &[
        EbiObjectImporter::StochasticDeterministicFiniteAutomaton(StochasticDeterministicFiniteAutomaton::import_as_object),
    ]
};

#[derive(Debug)]
pub struct StochasticDeterministicFiniteAutomaton {
    activity_key: ActivityKey,
    initial_state: usize,
    max_state: usize,
    sources: Vec<usize>,
    targets: Vec<usize>,
    activities: Vec<Activity>,
    probabilities: Vec<Fraction>
}

impl StochasticDeterministicFiniteAutomaton {

    pub fn new() -> Self {
        Self {
            activity_key: ActivityKey::new(),
            max_state: 0,
            initial_state: 0,
            sources: vec![],
            targets: vec![],
            activities: vec![],
            probabilities: vec![],
        }
    }

    pub fn get_activity_key(&mut self) -> &mut ActivityKey {
        &mut self.activity_key
    }

    pub fn get_sources(&self) -> &Vec<usize> {
        &self.sources
    }

    pub fn get_targets(&self) -> &Vec<usize> {
        &self.targets
    }

    pub fn get_activities(&self) -> &Vec<Activity> {
        &self.activities
    }

    pub fn set_initial_state(&mut self, state: usize) {
        self.max_state = max(self.max_state, state);
        self.initial_state = state;
    }

    pub fn add_transition(&mut self, source: usize, activity: Activity, target: usize, probability: Fraction) -> Result<()> {
        self.max_state = max(max(self.max_state, source), target);

        let (found, from) = self.binary_search(source, self.activity_key.get_id_from_activity(activity));
        if found {
            //edge already present
            Err(anyhow!("tried to insert an edge that would violate the determinism of the SDFA"))
        } else {
            self.sources.insert(from, source);
            self.targets.insert(from, target);
            self.activities.insert(from, activity);
            self.probabilities.insert(from, probability);
            Ok(())
        }
    }

    /**
     * Adds the probability to the transition. Returns the target state, which may be new.
     */
    pub fn take_or_add_transition(&mut self, source: usize, activity: Activity, probability: Fraction) -> usize {
        let (found, i) = self.binary_search(source, self.activity_key.get_id_from_activity(activity));
        if found {
            self.probabilities[i] += probability;
            return self.targets[i];
        } else {
            let target = self.add_state();
            self.sources.insert(i, source);
            self.targets.insert(i, target);
            self.activities.insert(i, activity);
            self.probabilities.insert(i, probability);
            return target;
        }
    }

    pub fn get_initial_state(&self) -> usize {
        self.initial_state
    }

    pub fn get_termination_probability(&self, state: usize) -> Fraction {
        let mut result = Fraction::one();

        let (_, pos_from) = self.binary_search(state, 0);
        let (found, mut pos_to) = self.binary_search(state, self.max_state);
        if found {
            pos_to += 1;
        }
        
        for i in pos_from .. pos_to {
            result -= &self.probabilities[i];
        }

        result
    }

    pub fn add_state(&mut self) -> usize {
		self.max_state += 1;
		return self.max_state;
	}

    fn compare(source1: usize, activity1: usize, source2: usize, activity2: Activity) -> Ordering {
		if source1 < source2 {
			return Ordering::Greater;
		} else if source1 > source2 {
			return Ordering::Less;
		} else if activity2 > activity1 {
			return Ordering::Greater;
		} else if activity2 < activity1 {
			return Ordering::Less;
		} else {
			return Ordering::Equal;
		}
	}

    fn binary_search(&self, source: usize, activity: usize) -> (bool, usize) {
        if self.sources.is_empty() {
            return (false, 0);
        }


        let mut size = self.sources.len();
        let mut left = 0;
        let mut right = size;
        while left < right {
            let mid = left + size / 2;

            let cmp = Self::compare(source, activity, self.sources[mid], self.activities[mid]);

            left = if cmp == Ordering::Less { mid + 1 } else { left };
            right = if cmp == Ordering::Greater { mid } else { right };
            if cmp == Ordering::Equal {
                assert!(mid < self.sources.len());
                return (true, mid);
            }

            size = right - left;
        }

        assert!(left <= self.sources.len());
        (false, left)
	}

    fn read_number(json: &Value, field: &str) -> Result<usize> {
        match &json[field] {
            Value::Null => return Err(anyhow!("field not found")),
            Value::Bool(_) => return Err(anyhow!("field is a boolean, where number expected")),
            Value::Number(n) => {
                if !n.is_u64() {
                    return Err(anyhow!("number is not an integer"))
                }
                return Ok(usize::try_from(n.as_u64().unwrap())?);
            },
            Value::String(_) => return Err(anyhow!("field is a literal, where number expected")),
            Value::Array(_) => return Err(anyhow!("field is a list, where number expected")),
            Value::Object(_) => return Err(anyhow!("field is an object, where number expected")),
        }
    }

    fn read_fraction(json: &Value, field: &str) -> Result<Fraction> {
        match &json[field] {
            Value::Null => return Err(anyhow!("field not found")),
            Value::Bool(_) => return Err(anyhow!("field is a boolean, where fraction expected")),
            Value::Number(n) => return Ok(n.to_string().parse::<Fraction>()?),
            Value::String(s) => return Ok(s.parse::<Fraction>()?),
            Value::Array(_) => return Err(anyhow!("field is a list, where fraction expected")),
            Value::Object(_) => return Err(anyhow!("field is an object, where fraction expected")),
        }
    }

    fn read_list<'a>(json: &'a Value, field: &str) -> Result<&'a Vec<Value>> {
        match &json[field] {
            Value::Null => return Err(anyhow!("field not found")),
            Value::Bool(_) => return Err(anyhow!("field is a boolean, where list expected")),
            Value::Number(_) => return Err(anyhow!("field is a number, where list expected")),
            Value::String(_) => return Err(anyhow!("field is a literal, where list expected")),
            Value::Array(arr) => return Ok(&arr),
            Value::Object(_) => return Err(anyhow!("field is an object, where list expected")),
        }
    }

    fn read_string<'a>(json: &'a Value, field: &str) -> Result<String> {
        match &json[field] {
            Value::Null => return Err(anyhow!("field not found")),
            Value::Bool(_) => return Err(anyhow!("field is a boolean, where literal expected")),
            Value::Number(n) => return Ok(n.to_string()),
            Value::String(s) => return Ok(s.to_string()),
            Value::Array(_) => return Err(anyhow!("field is a list, where literal expected")),
            Value::Object(_) => return Err(anyhow!("field is an object, where literal expected")),
        }
    }

    pub fn get_p_semantics(net: Rc<Self>) -> Result<Box<dyn StochasticDeterministicSemantics<PState = usize>>> {
        Ok(Box::new(StochasticDeterministicFiniteAutomatonSemantics::new(net)))
    }

    pub fn import_as_p_semantics(reader: &mut dyn BufRead) -> Result<EbiTraitStochasticDeterministicSemantics> {
        log::info!("convert SDFA to stochastic deterministic semantics");
        let sdfa = StochasticDeterministicFiniteAutomaton::import(reader)?;
        let s = Rc::new(sdfa);
        Ok(EbiTraitStochasticDeterministicSemantics::Usize(Self::get_p_semantics(s)?))
    }
    
    pub fn to_stochastic_labelled_petri_net(self) -> StochasticLabelledPetriNet {
        log::info!("convert SDFA to stochastic labelled Petri net");
        let activity_key = self.activity_key;
        let mut transitions = vec![];
        let mut weights = vec![];
        let mut places = 2 + self.max_state; //0 = source, 1 = sink
        let mut initial_marking = Marking::new();

        //add edges
        let mut source_probability = Fraction::one();
        if let Some(mut source_last) = self.sources.get(0) {
            for (source, (target, (activity, probability))) in self.sources.iter().zip(self.targets.iter().zip(self.activities.iter().zip(self.probabilities.iter()))) {
                //add finalisation transition
                if source_last != source {
                    if !source_probability.is_zero() {
                        let mut transition = Transition::new_silent(transitions.len());
                        transition.incoming.push(2 + source_last);
                        transition.outgoing.push(1);
                        transitions.push(transition);
                        weights.push(source_probability);
                    }

                    source_probability = Fraction::one();
                    source_last = source;
                }
                
                //add transition
                let mut transition = Transition::new_labelled(transitions.len(), *activity);
                transition.incoming.push(2 + source);
                transition.outgoing.push(2 + target);
            }
        }

        StochasticLabelledPetriNet::from_fields(activity_key, places, transitions, initial_marking, weights)
    }

}

impl FromStr for StochasticDeterministicFiniteAutomaton {
    type Err = Error;

    fn from_str(s: &str) -> std::prelude::v1::Result<Self, Self::Err> {
        let mut reader = io::Cursor::new(s);
        Self::import(&mut reader)
    }
}

impl Importable for StochasticDeterministicFiniteAutomaton {

    fn import_as_object(reader: &mut dyn BufRead) -> Result<EbiObject> {
        Ok(EbiObject::StochasticDeterministicFiniteAutomaton(Self::import(reader)?))
    }

    fn import(reader: &mut dyn BufRead) -> Result<Self> where Self: Sized {
        let json: Value = serde_json::from_reader(reader)?;

        let mut result = StochasticDeterministicFiniteAutomaton::new();

        result.set_initial_state(Self::read_number(&json, "initialState").context("failed to read initial state")?);
        let jtrans = Self::read_list(&json, "transitions").context("failed to read list of transitions")?;
        for jtransition in jtrans {
            let from = Self::read_number(jtransition, "from").context("could not read from")?;
            let to = Self::read_number(jtransition, "to").context("could not read to")?;
            let label = Self::read_string(jtransition, "label").context("could not read label")?;
            let activity = result.activity_key.process_activity(label.as_str());
            let probability = Self::read_fraction(jtransition, "prob").context("could not read probability")?;

            result.add_transition(from, activity, to, probability)?;
        }

        return Ok(result);
    }
}

fn find_difference(vector1: &[usize], vector2: &[usize]) -> Vec<usize> {
    let mut difference = Vec::new();

    for &element in vector1 {
        if !vector2.contains(&element) {
            difference.push(element);
        }
    }
    difference
}

impl EbiTraitQueriableStochasticLanguage for StochasticDeterministicFiniteAutomaton {

    fn get_sample(&mut self, number_of_traces: u64) -> Result<FiniteStochasticLanguage> {
        let mut result = HashMap::new();

        let mut trace_num_map = HashMap::new();
        let mut trace_left = number_of_traces;

        let index2activity = self.get_activity_key().activity2name.clone();

        while trace_left > 0 {
            let mut current_state = self.get_initial_state();
            let mut trace = vec![];
            let mut is_final = false;
            
            loop {
                // if current_state is not in sources, break
                if !self.sources.contains(&current_state) {
                    break;
                }
            // find all possible transitions from the current state
            let mut possible_transitions = vec![];
            let mut possible_prob = vec![];
            let mut real_prob = Vec::new();

            let mut prob_map = HashMap::new();

            let mut temp_i = 0;
            for (idx, &source) in self.sources.iter().enumerate() {
                if source == current_state {
                    possible_transitions.push(idx);
                    possible_prob.push(idx);
                    real_prob.push(self.probabilities[idx].clone());
                    prob_map.insert(temp_i, idx);
                    temp_i+=1
                }
            }
            // if the sum of element in real_prob is less than 1, add the difference to the last element
            let mut sum = Fraction::zero();
            for i in real_prob.iter(){
                sum = sum + i.clone();
            }

            let mut sum_less_than_1 = false;
            if sum < Fraction::one() {
                let diff = Fraction::one() - sum;
                real_prob.push(diff);
                sum_less_than_1 = true;
            }


            let sample_idx = Fraction::choose_randomly(&real_prob)?;
            if sum_less_than_1 && sample_idx == real_prob.len() - 1 {
                break;
            }

            // choose one transition randomly
            let chosen_transition_idx = prob_map.get(&sample_idx).unwrap();

            // update the current state
            let next_state = self.targets.get(*chosen_transition_idx).unwrap();

            // trace.push(self.activity_key[self.activities[*chosen_transition_idx]]);
            trace.push(self.activity_key.get_activity_label(&self.activities[*chosen_transition_idx]).to_string());
            current_state = *next_state;
            }
            if let Some(entry) = trace_num_map.get_mut(&trace) {
                *entry += 1;
            }
            else{
                trace_num_map.insert(trace, 1);
            }
            trace_left -= 1;
        } 

        for (trace, num) in trace_num_map.iter(){
            let mut trace_prob = Fraction::from((*num, number_of_traces));
            result.insert(trace.clone(), trace_prob);
        }

        if result.is_empty() {
            return Err(anyhow!("Sampling returned an empty language; there are no traces in the model."));
        }

        Ok(FiniteStochasticLanguage::new(result))
    }

    fn get_probability(&mut self, follower: &FollowerSemantics) -> Result<Fraction> {
        match follower {
            FollowerSemantics::Trace(trace) => {
                let mut state = self.get_initial_state();
                let mut result = Fraction::one();

                for activity in trace.iter() {
                    let i = self.activity_key.process_activity(activity);
                    let (found, pos) = self.binary_search(state, self.activity_key.get_id_from_activity(i));
                    if !found {
                        return Ok(Fraction::zero());
                    }
                    state = self.targets[pos];
                    result *= self.probabilities[pos].clone();
                }

                let mut not_real_final = false;
                // iterate sources and check whether the state is a partial-final
                for (i,source) in self.sources.iter().enumerate(){
                    if state == *source{
                        not_real_final = true;
                        break;
                    }
                }

                if not_real_final{
                    let mut possible_transitions = vec![];
                    let mut possible_prob = vec![];
                    let mut real_prob = Vec::new();
                    let mut prob_map = HashMap::new();

                    let mut temp_i = 0;
                    for (idx, &source) in self.sources.iter().enumerate() {
                        if source == state {
                            possible_transitions.push(idx);
                            possible_prob.push(idx);
                            real_prob.push(self.probabilities[idx].clone());
                            prob_map.insert(temp_i, idx);
                            temp_i+=1
                        }
                    }
                    // if the sum of element in real_prob is less than 1, add the difference to the last element
                    let mut sum = Fraction::zero();
                    for i in real_prob.iter(){
                        sum = sum + i.clone();
                    }

                    let mut sum_less_than_1 = false;
                    if sum < Fraction::one(){
                        let diff = Fraction::one() - sum;
                        result = result * diff;
                    }
                }


                Ok(result)
            },
        }
    }


}

impl Exportable for StochasticDeterministicFiniteAutomaton {
    fn export(&self, f: &mut impl std::io::Write) -> Result<()> {
        Ok(write!(f, "{}", self)?)
    }
}

impl Infoable for StochasticDeterministicFiniteAutomaton {
    fn info(&self, f: &mut impl std::io::Write) -> Result<()> {
        writeln!(f, "Number of states\t{}", self.max_state)?;
        writeln!(f, "Number of transitions\t{}", self.sources.len())?;
        writeln!(f, "Number of activities\t{}", self.activity_key.activity2name.len())?;

        Ok(write!(f, "")?)
    }
}

impl fmt::Display for StochasticDeterministicFiniteAutomaton {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "{{")?;
        writeln!(f, "\"initialState\": {},", self.get_initial_state())?;
        writeln!(f, "\"transitions\": [")?;
        for pos in 0..self.sources.len() {
            write!(f, "{{\"from\":{},\"to\":{},\"label\":\"{}\",\"prob\":\"{}\"}}", 
                self.sources[pos], 
                self.targets[pos], 
                self.activity_key.get_activity_label(&self.activities[pos]),
                self.probabilities[pos])?;
            if pos + 1 == self.sources.len() {
                writeln!(f, "")?;
            } else {
                writeln!(f, ",")?;
            }
        }
        writeln!(f, "]}}")?;
        Ok(())
    }
}

impl Dottable for StochasticDeterministicFiniteAutomaton {
    fn to_dot(&self) -> layout::topo::layout::VisualGraph {
        let mut graph = VisualGraph::new(layout::core::base::Orientation::LeftToRight);

        let mut places = vec![];
        for _ in 0 ..= self.max_state {
            places.push(<dyn Dottable>::create_place(&mut graph, ""));
        }

        for pos in 0..self.sources.len() {
            let source = places[self.sources[pos]];
            let target = places[self.targets[pos]];
            let probability = &self.probabilities[pos];
            let activity = self.activity_key.get_activity_label(&self.activities[pos]);
            
            <dyn Dottable>::create_edge(&mut graph, &source, &target, &format!("{}, {}", activity, probability.to_string()));
        }

        return graph;
    }
}

struct StochasticDeterministicFiniteAutomatonSemantics {
    sdfa: Rc<StochasticDeterministicFiniteAutomaton>
}

impl StochasticDeterministicFiniteAutomatonSemantics {
    pub fn new(sdfa: Rc<StochasticDeterministicFiniteAutomaton>) -> Self {
        Self {
            sdfa: sdfa
        }
    }
}

impl StochasticDeterministicSemantics for StochasticDeterministicFiniteAutomatonSemantics {
    type PState = usize;

    fn get_initial_state(&self) -> Result<Self::PState> {
        Ok(self.sdfa.get_initial_state())
    }

    fn execute_activity(&self, state: &Self::PState, activity: Activity) -> Result<Self::PState> {
        let (found, i) = self.sdfa.binary_search(*state, self.sdfa.activity_key.get_id_from_activity(activity));
        if found{
            Ok(self.sdfa.targets[i])
        } else {
            Err(anyhow!("activity not enabled"))
        }
    }

    fn get_termination_probability(&self, state: &Self::PState) -> Fraction {
        self.sdfa.get_termination_probability(*state)
    }

    fn get_activity_probability(&self, state: &Self::PState, activity: Activity) -> Fraction {
        let (found, i) = self.sdfa.binary_search(*state, self.sdfa.activity_key.get_id_from_activity(activity));
        match found {
            true => self.sdfa.probabilities[i].clone(),
            false => Fraction::zero(),
        }
    }

    fn get_activity_label(&self, activity: &Activity) -> &str {
        self.sdfa.activity_key.get_activity_label(&activity)
    }

    fn get_enabled_activities(&self, state: &Self::PState) -> Vec<Activity> {
        let mut result = vec![];

        let (_, mut i) = self.sdfa.binary_search(*state, 0);
        while i < self.sdfa.activities.len() && self.sdfa.sources[i] == *state {
            result.push(self.sdfa.activities[i]);
            i += 1;
        }

        return result;
    }
}

impl<'a> IntoIterator for &'a StochasticDeterministicFiniteAutomaton {
    type Item = (&'a usize, &'a usize, &'a Activity, &'a Fraction);

    type IntoIter = StochasticDeterministicFiniteAutomatonIterator<'a>;

    fn into_iter(self) -> Self::IntoIter {
        Self::IntoIter {
            it_sources:  self.sources.iter(),
            it_targets:  self.targets.iter(),
            it_activities: self.activities.iter(),
            it_probabilities: self.probabilities.iter()
        }
    }
}

pub struct StochasticDeterministicFiniteAutomatonIterator<'a> {
    it_sources: std::slice::Iter<'a, usize>,
    it_targets: std::slice::Iter<'a, usize>,
    it_activities: std::slice::Iter<'a, Activity>,
    it_probabilities: std::slice::Iter<'a, Fraction>
}

impl<'a> Iterator for StochasticDeterministicFiniteAutomatonIterator<'a> {
    type Item = (&'a usize, &'a usize, &'a Activity, &'a Fraction);

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(source) = self.it_sources.next() {
            let target = self.it_targets.next().unwrap();
            let activity = self.it_activities.next().unwrap();
            let probability = self.it_probabilities.next().unwrap();
            let result = Some((source, target, activity, probability));
            result
        } else {
            None
        }
    }
}

impl<'a> IntoIterator for &'a mut StochasticDeterministicFiniteAutomaton {
    type Item = (&'a usize, &'a usize, &'a Activity, &'a mut Fraction);

    type IntoIter = StochasticDeterministicFiniteAutomatonMutIterator<'a>;

    fn into_iter(self) -> Self::IntoIter {
        Self::IntoIter {
            it_sources:  self.sources.iter(),
            it_targets:  self.targets.iter(),
            it_activities: self.activities.iter(),
            it_probabilities: self.probabilities.iter_mut()
        }
    }
}

pub struct StochasticDeterministicFiniteAutomatonMutIterator<'a> {
    it_sources: std::slice::Iter<'a, usize>,
    it_targets: std::slice::Iter<'a, usize>,
    it_activities: std::slice::Iter<'a, Activity>,
    it_probabilities: std::slice::IterMut<'a, Fraction>
}

impl<'a> Iterator for StochasticDeterministicFiniteAutomatonMutIterator<'a> {
    type Item = (&'a usize, &'a usize, &'a Activity, &'a mut Fraction);

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(source) = self.it_sources.next() {
            let target = self.it_targets.next().unwrap();
            let activity = self.it_activities.next().unwrap();
            let probability = self.it_probabilities.next().unwrap();
            let result = Some((source, target, activity, probability));
            result
        } else {
            None
        }
    }
}