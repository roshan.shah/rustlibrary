use core::fmt;
use std::{any, collections::{HashMap, HashSet}, fmt::Display, fs::File, io::{BufRead, BufReader}, rc::Rc};
use anyhow::{anyhow, Context, Result};
use chrono::{DateTime, Utc};
use flate2::bufread::GzDecoder;
use fraction::One;
use process_mining::{event_log::{event_log_struct::EventLogClassifier, export_xes::{self, export_xes_event_log_to_file}, AttributeValue}, XESImportOptions};
use quick_xml::Writer;

use crate::{activity_key::{Activity, ActivityKey}, analysis::PSemanticsAnalyser, ebi_commands::ebi_command_info::Infoable, ebi_traits::{ebi_trait_event_log::EbiTraitEventLog, ebi_trait_finite_language::EbiTraitFiniteLanguage, ebi_trait_finite_stochastic_language::EbiTraitFiniteStochasticLanguage, ebi_trait_iterable_stochastic_language::EbiTraitIterableStochasticLanguage, ebi_trait_queriable_stochastic_language::EbiTraitQueriableStochasticLanguage, ebi_trait_stochastic_deterministic_semantics::EbiTraitStochasticDeterministicSemantics}, export::Exportable, file_handler::EbiFileHandler, import::{self, EbiObjectImporter, EbiTraitImporter, Importable}, math::fraction::Fraction, Trace};

use super::{ebi_object::EbiObject, finite_language::FiniteLanguage, finite_stochastic_language::FiniteStochasticLanguage, stochastic_deterministic_finite_automaton::StochasticDeterministicFiniteAutomaton};

pub const EBI_EVENT_LOG: EbiFileHandler = EbiFileHandler {
    name: "event log",
    article: "an",
    file_extension: "xes",
    validator: import::validate::<EventLog>,
    trait_importers: &[
        EbiTraitImporter::FiniteLanguage(EventLog::read_as_finite_language),
        EbiTraitImporter::FiniteStochasticLanguage(EventLog::read_as_finite_stochastic_language),
        EbiTraitImporter::QueriableStochasticLanguage(EventLog::read_as_queriable_stochastic_language),
        EbiTraitImporter::IterableStochasticLanguage(EventLog::read_as_iterable_stochastic_language),
        EbiTraitImporter::EventLog(EventLog::read_as_event_log),
        EbiTraitImporter::StochasticDeterministicSemantics(EventLog::read_as_stochastic_deterministic_semantics)
    ],
    object_importers: &[
        EbiObjectImporter::EventLog(EventLog::import_as_object),
    ]
};

pub struct EventLog {
    classifier: EventLogClassifier,
    log: process_mining::EventLog,
    activity_key: ActivityKey
}

impl EventLog {
    pub fn new(log: process_mining::EventLog, classifier: EventLogClassifier) -> Self {
        Self {
            classifier: classifier,
            log: log,
            activity_key: ActivityKey::new()
        }
    }

    pub fn read_as_finite_language(reader: &mut dyn BufRead) -> Result<Box<dyn EbiTraitFiniteLanguage>> {
        let event_log = EventLog::import(reader)?;
        Ok(Box::new(event_log.to_finite_language()))
    }

    pub fn read_as_finite_stochastic_language(reader: &mut dyn BufRead) -> Result<Box<dyn EbiTraitFiniteStochasticLanguage>> {
        let event_log = EventLog::import(reader)?;
        Ok(Box::new(Into::<FiniteStochasticLanguage>::into(event_log.to_finite_stochastic_language())))
    }

    pub fn read_as_queriable_stochastic_language(reader: &mut dyn BufRead) -> Result<Box<dyn EbiTraitQueriableStochasticLanguage>> {
        let event_log = EventLog::import(reader)?;
        Ok(Box::new(event_log.to_finite_stochastic_language()))
    }

    pub fn read_as_iterable_stochastic_language(reader: &mut dyn BufRead) -> Result<Box<dyn EbiTraitIterableStochasticLanguage>> {
        let event_log = EventLog::import(reader)?;
        Ok(Box::new(Into::<FiniteStochasticLanguage>::into(event_log.to_finite_stochastic_language())))
    }

    pub fn read_as_event_log(reader: &mut dyn BufRead) -> Result<Box<dyn EbiTraitEventLog>> {
        let event_log = EventLog::import(reader)?;
        Ok(Box::new(event_log))
    }

    pub fn read_as_stochastic_deterministic_semantics(reader: &mut dyn BufRead) -> Result<EbiTraitStochasticDeterministicSemantics> {
        let mut event_log = EventLog::import(reader)?;
        let sdfa = event_log.to_stochastic_deterministic_finite_automaton();
        let semantics = StochasticDeterministicFiniteAutomaton::get_p_semantics(Rc::new(sdfa))?;
        Ok(EbiTraitStochasticDeterministicSemantics::Usize(semantics))
    }

    pub fn to_finite_language(&self) -> FiniteLanguage {
        log::info!("create finite language");

        let mut map: HashSet<Trace> = HashSet::new();
        for t in &self.log.traces {
            let trace = t.events.iter().map(|event| self.classifier.get_class_identity(event)).collect::<Vec<String>>();
            map.insert(trace);
        }

        FiniteLanguage::from(map)
    }

    pub fn to_finite_stochastic_language(&self) -> FiniteStochasticLanguage {
        log::info!("create stochastic language");
        let mut map: HashMap<Trace, Fraction> = HashMap::new();
        for t in &self.log.traces {
            let trace = t.events.iter().map(|event| self.classifier.get_class_identity(event)).collect::<Vec<String>>();
            match map.entry(trace) {
                std::collections::hash_map::Entry::Occupied(mut e) => {*e.get_mut() += Fraction::one();()},
                std::collections::hash_map::Entry::Vacant(e) => {e.insert(Fraction::one());()},
            }
        }

        FiniteStochasticLanguage::new(map)
    }

    pub fn to_stochastic_deterministic_finite_automaton(&mut self) -> StochasticDeterministicFiniteAutomaton {
        log::info!("convert event log to SDFA");

        let mut result = StochasticDeterministicFiniteAutomaton::new();
        let mut final_states = HashMap::new();

        //create automaton
        for trace_index in 0..self.log.traces.len() {
            let trace = self.read_trace_with_activity_key(result.get_activity_key(), &trace_index);
            let mut state = result.get_initial_state();

            for activity in trace {
                state = result.take_or_add_transition(state, activity, Fraction::one());
            }

            match final_states.entry(state) {
                std::collections::hash_map::Entry::Occupied(mut e) => {*e.get_mut() += Fraction::one()},
                std::collections::hash_map::Entry::Vacant(e) => {e.insert(Fraction::one());},
            }
        }

        log::debug!("SDFA has {} states", result.get_sources().len());

        //count
        let mut sums = final_states;
        for (source, _, _, probability) in &result {
            match sums.entry(*source) {
                std::collections::hash_map::Entry::Occupied(mut e) => {*e.get_mut() += probability},
                std::collections::hash_map::Entry::Vacant(e) => {e.insert(probability.clone());},
            }
        }

        log::debug!("SDFA sources: {:?}", result.get_sources());
        log::debug!("SDFA activities: {:?}", result.get_activities());
        log::debug!("SDFA targets: {:?}", result.get_sources());

        //normalise
        for (source, _, _, probability) in &mut result {
            *probability /= sums.get(source).unwrap();
        }

        result
    }
}

impl Importable for EventLog {
    fn import_as_object(reader: &mut dyn BufRead) -> anyhow::Result<EbiObject> {
        Ok(EbiObject::EventLog(Self::import(reader)?))
    }

    fn import(reader: &mut dyn BufRead) -> anyhow::Result<Self> where Self: Sized {
        //let mut reader2 = quick_xml::Reader::from_reader(reader);
        let log = process_mining::event_log::import_xes::import_xes(reader, XESImportOptions::default());
        if log.is_err() {
            return Err(anyhow!("{}", log.err().unwrap()))
        }
        let log = log.unwrap();
        let classifier = EventLogClassifier{name: "concept:name".to_string(), keys: vec!["concept:name".to_string()]};
        if log.traces.is_empty() {
            return Err(anyhow!("event log has no traces"));
        }
        Ok(EventLog::new(log, classifier))
    }
}

impl Exportable for EventLog {
    fn export(&self, f: &mut impl std::io::Write) -> Result<()> {
        let mut writer = Writer::new(f);
        export_xes::export_xes_event_log(&mut writer, &self.log)?;
        Ok(())
    }
}

impl fmt::Display for EventLog {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "event log with {} traces", self.log.traces.len())
    }
}

impl Infoable for EventLog {
    fn info(&self, f: &mut impl std::io::Write) -> Result<()> {
        writeln!(f, "Number of traces\t{}", self.log.traces.len())?;
        writeln!(f, "Number of events\t{}", self.log.traces.iter().map(|t| t.events.len()).sum::<usize>())?;

        let trace_atts = self.gather_trace_attributes();
        let t: Vec<String> = trace_atts.iter().map(|(att, data_type)| format!("{}\t{}", att, data_type) ).collect();
        writeln!(f, "Trace attributes:")?;
        writeln!(f, "\t{}", t.join("\n\t"))?;

        Ok(write!(f, "")?)
    }
}

impl EbiTraitEventLog for EventLog {
    fn get_log(&self) -> &process_mining::EventLog {
        &self.log
    }

    fn gather_trace_attributes(&self) -> HashMap<String, DataType> {
        let mut map: HashMap<String, DataType> = HashMap::new();
        for trace in &self.log.traces {
            for attribute in &trace.attributes {
                match map.entry(attribute.key.clone()) {
                    std::collections::hash_map::Entry::Occupied(mut e) => {e.get_mut().update(&attribute.value);()},
                    std::collections::hash_map::Entry::Vacant(e) => {e.insert(DataType::init(&attribute.value));()},
                }
            }
        }
        map
    }

    fn get_number_of_traces(&self) -> usize {
        self.log.traces.len()
    }

    fn traces(&self) -> &Vec<process_mining::event_log::Trace> {
        &self.log.traces
    }

    fn read_trace(&mut self, trace_index: &usize) -> Vec<Activity> {
        self.log.traces[*trace_index].events.iter().map(|event| self.activity_key.process_activity(&self.classifier.get_class_identity(event))).collect::<Vec<Activity>>()
    }

    fn read_trace_with_activity_key(&self, activity_key: &mut ActivityKey, trace_index: &usize) -> Vec<Activity> {
        self.log.traces[*trace_index].events.iter().map(|event| activity_key.process_activity(&self.classifier.get_class_identity(event))).collect::<Vec<Activity>>()
    }
}

#[derive(Debug)]
pub enum DataType {
    Categorical,
    Numerical(Fraction, Fraction), //minimum, maximum
    Time(DateTime<Utc>, DateTime<Utc>), //minimum, maximum
    Undefined
}

impl DataType {
    fn init(value: &AttributeValue) -> Self {
        match value {
            AttributeValue::String(x) => 
                if let Ok(v) = x.parse::<Fraction>() {
                    Self::Numerical(v.clone(), v)
                } else if let Ok(v) = x.parse::<DateTime<Utc>>() {
                    Self::Time(v, v)
                } else {
                    Self::Categorical
                },
            AttributeValue::Date(x) => Self::Time(*x, *x),
            AttributeValue::Int(x) => Self::Numerical(Fraction::from(*x), Fraction::from(*x)),
            AttributeValue::Float(x) => Self::Numerical(x.to_string().parse::<Fraction>().unwrap(), x.to_string().parse::<Fraction>().unwrap()),
            AttributeValue::Boolean(_) => Self::Undefined,
            AttributeValue::ID(_) => Self::Undefined,
            AttributeValue::List(_) => Self::Undefined,
            AttributeValue::Container(_) => Self::Undefined,
            AttributeValue::None() => Self::Undefined,
        }
    }

    fn update(&mut self, value: &AttributeValue) {
        *self = match (&self, value) {
            (DataType::Categorical, AttributeValue::String(_)) => Self::Categorical,
            (DataType::Categorical, AttributeValue::Date(_)) => Self::Undefined,
            (DataType::Categorical, AttributeValue::Int(_)) => Self::Undefined,
            (DataType::Categorical, AttributeValue::Float(_)) => Self::Undefined,
            (DataType::Categorical, AttributeValue::Boolean(_)) => Self::Undefined,
            (DataType::Categorical, AttributeValue::ID(_)) => Self::Undefined,
            (DataType::Categorical, AttributeValue::List(_)) => Self::Undefined,
            (DataType::Categorical, AttributeValue::Container(_)) => Self::Undefined,
            (DataType::Categorical, AttributeValue::None()) => Self::Categorical,
            (DataType::Numerical(min, max), AttributeValue::String(s)) => {
                if let Ok(x1) = s.parse::<Fraction>() {Self::Numerical(x1.clone().min(min.clone()), x1.max(max.clone()))} else {Self::Undefined}
            },
            (DataType::Numerical(_, _), AttributeValue::Date(_)) => Self::Undefined,
            (DataType::Numerical(min, max), AttributeValue::Int(y)) => Self::Numerical(min.min(&mut Fraction::from(*y)).clone(), max.max(&mut Fraction::from(*y)).clone()),
            (DataType::Numerical(min, max), AttributeValue::Float(y)) => Self::Numerical(min.min(&mut y.to_string().parse::<Fraction>().unwrap()).clone(), max.max(&mut y.to_string().parse::<Fraction>().unwrap()).clone()),
            (DataType::Numerical(_, _), AttributeValue::Boolean(_)) => Self::Undefined,
            (DataType::Numerical(_, _), AttributeValue::ID(_)) => Self::Undefined,
            (DataType::Numerical(_, _), AttributeValue::List(_)) => Self::Undefined,
            (DataType::Numerical(_, _), AttributeValue::Container(_)) => Self::Undefined,
            (DataType::Numerical(min, max), AttributeValue::None()) => Self::Numerical(min.clone(), max.clone()),
            (DataType::Time(min, max), AttributeValue::String(s)) => 
                if let Ok(v) = s.parse::<DateTime<Utc>>() {
                    Self::Time(v.min(*min), v.max(*max))
                } else {
                    Self::Categorical
                },
            (DataType::Time(min, max), AttributeValue::Date(y)) => Self::Time(*min.min(y), *max.max(y)),
            (DataType::Time(_, _), AttributeValue::Int(_)) => Self::Undefined,
            (DataType::Time(_, _), AttributeValue::Float(_)) => Self::Undefined,
            (DataType::Time(_, _), AttributeValue::Boolean(_)) => Self::Undefined,
            (DataType::Time(_, _), AttributeValue::ID(_)) => Self::Undefined,
            (DataType::Time(_, _), AttributeValue::List(_)) => Self::Undefined,
            (DataType::Time(_, _), AttributeValue::Container(_)) => Self::Undefined,
            (DataType::Time(x, y), AttributeValue::None()) => Self::Time(*x, *y),
            (DataType::Undefined, AttributeValue::String(_)) => Self::Undefined,
            (DataType::Undefined, AttributeValue::Date(_)) => Self::Undefined,
            (DataType::Undefined, AttributeValue::Int(_)) => Self::Undefined,
            (DataType::Undefined, AttributeValue::Float(_)) => Self::Undefined,
            (DataType::Undefined, AttributeValue::Boolean(_)) => Self::Undefined,
            (DataType::Undefined, AttributeValue::ID(_)) => Self::Undefined,
            (DataType::Undefined, AttributeValue::List(_)) => Self::Undefined,
            (DataType::Undefined, AttributeValue::Container(_)) => Self::Undefined,
            (DataType::Undefined, AttributeValue::None()) => Self::Undefined,
        };
    }
}

impl Display for DataType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            DataType::Categorical => write!(f, "categorical"),
            DataType::Numerical(min, max) => write!(f, "numerical between {} and {}", min, max),
            DataType::Time(min, max) => write!(f, "time between {} and {}", min, max),
            DataType::Undefined => write!(f, "undefined"),
        }
    }
}