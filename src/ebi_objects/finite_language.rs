use crate::{ebi_commands::ebi_command_info::Infoable, ebi_traits::ebi_trait_finite_language::EbiTraitFiniteLanguage, export::Exportable, file_handler::EbiFileHandler, import::{self, EbiObjectImporter, EbiTraitImporter, Importable}, line_reader::LineReader, Trace};
use anyhow::{anyhow, Result, Context};
use std::{collections::{hash_set::Iter, HashSet}, fmt::Display, io::BufRead};

use super::ebi_object::EbiObject;

pub const HEADER: &str = "finite language";

pub const EBI_FINITE_LANGUAGE: EbiFileHandler = EbiFileHandler {
    name: "finite language",
    article: "a",
    file_extension: "lang",
    validator: import::validate::<FiniteLanguage>,
    trait_importers: &[
        EbiTraitImporter::FiniteLanguage(import::read_as_finite_language::<FiniteLanguage>),
    ],
    object_importers: &[
        EbiObjectImporter::FiniteLanguage(FiniteLanguage::import_as_object),
    ]
};

pub struct FiniteLanguage {
    traces: HashSet<Trace>
}

impl FiniteLanguage {
    pub fn new() -> FiniteLanguage {
        Self {
            traces: HashSet::<Trace>::new()
        }
    }

    pub fn push(&mut self, trace: Trace) {
        self.traces.insert(trace);
    }
}

impl EbiTraitFiniteLanguage for FiniteLanguage {
    fn len(&self) -> usize {
        self.traces.len()
    }

    fn iter(&self) -> Iter<'_, Trace> {
        self.traces.iter()
    }
}

impl Exportable for FiniteLanguage {
    fn export(&self, f: &mut impl std::io::Write) -> Result<()> {
        Ok(write!(f, "{}", self)?)
    }
}

impl Infoable for FiniteLanguage {
    fn info(&self, f: &mut impl std::io::Write) -> Result<()> {
        writeln!(f, "Number of traces\t{}", self.traces.len())?;
        writeln!(f, "Number of events\t{}", self.traces.iter().map(|t| t.len()).sum::<usize>())?;

        Ok(write!(f, "")?)
    }
}

impl Display for FiniteLanguage {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "{}", HEADER)?;
        writeln!(f, "# number of traces\n{}", self.len())?;

        for (pos, trace) in self.iter().enumerate() {
            writeln!(f, "# trace {}", pos)?;

            writeln!(f, "# number of events\n{}", trace.len())?;
            for event in trace {
                writeln!(f, "{}", event)?;
            }
        }

        write!(f, "")
    }
}

impl Importable for FiniteLanguage {
    
    fn import_as_object(reader: &mut dyn BufRead) -> Result<EbiObject> {
        Ok(EbiObject::FiniteLanguage(Self::import(reader)?))
    }

    fn import(reader: &mut dyn BufRead) -> Result<Self> {
        let mut lreader = LineReader::new(reader);

        let head = lreader.next_line_string().with_context(|| format!("failed to read header, which should be `{}`", HEADER))?;
        if head != HEADER {
            return Err(anyhow!("first line should be exactly `{}`, but found `{}`", HEADER, head));
        }

        let number_of_traces = lreader.next_line_index().context("failed to read number of places")?;
        if number_of_traces == 0 {
            return Err(anyhow!("language is empty"));
        }

        let mut traces = HashSet::new();
        for trace_i in 0 .. number_of_traces {
            let number_of_events = lreader.next_line_index().with_context(|| format!("failed to read number of events for trace {} at line {}", trace_i, lreader.get_last_line_number()))?;

            let mut trace = vec![];
            trace.reserve_exact(number_of_events);

            for event_i in 0 .. number_of_events {
                let event = lreader.next_line_string().with_context(|| format!("failed to read event {} of trace {} at line {}", event_i, trace_i, lreader.get_last_line_number()))?;
                trace.push(event);
            }

            if !traces.insert(trace) {
                return Err(anyhow!("trace {} ending at line {} appears twice in language", trace_i, lreader.get_last_line_number()));    
            }
        }

        Ok(Self {
            traces: traces
        })
    }
}

impl From<HashSet<Trace>> for FiniteLanguage {
    fn from(value: HashSet<Trace>) -> Self {
        Self { traces: value }
    }
}