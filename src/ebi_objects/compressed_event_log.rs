use std::{io::{BufRead, BufReader}, rc::Rc};
use anyhow::Result;
use flate2::bufread::GzDecoder;

use crate::{ebi_traits::{ebi_trait_event_log::EbiTraitEventLog, ebi_trait_finite_language::EbiTraitFiniteLanguage, ebi_trait_finite_stochastic_language::EbiTraitFiniteStochasticLanguage, ebi_trait_iterable_stochastic_language::EbiTraitIterableStochasticLanguage, ebi_trait_queriable_stochastic_language::EbiTraitQueriableStochasticLanguage, ebi_trait_stochastic_deterministic_semantics::EbiTraitStochasticDeterministicSemantics}, file_handler::EbiFileHandler, import::{self, EbiObjectImporter, EbiTraitImporter, Importable}};

use super::{ebi_object::EbiObject, event_log::EventLog, finite_stochastic_language::FiniteStochasticLanguage, stochastic_deterministic_finite_automaton::StochasticDeterministicFiniteAutomaton};

pub const EBI_COMPRESSED_EVENT_LOG: EbiFileHandler = EbiFileHandler {
    name: "compressed event log",
    article: "a",
    file_extension: "xes.gz",
    validator: import::validate::<CompressedEventLog>,
    trait_importers: &[
        EbiTraitImporter::FiniteLanguage(CompressedEventLog::read_as_finite_language),
        EbiTraitImporter::FiniteStochasticLanguage(CompressedEventLog::read_as_finite_stochastic_language),
        EbiTraitImporter::QueriableStochasticLanguage(CompressedEventLog::read_as_queriable_stochastic_language),
        EbiTraitImporter::IterableStochasticLanguage(CompressedEventLog::read_as_iterable_stochastic_language),
        EbiTraitImporter::EventLog(CompressedEventLog::read_as_event_log),
        EbiTraitImporter::StochasticDeterministicSemantics(CompressedEventLog::read_as_stochastic_deterministic_semantics)
    ],
    object_importers: &[
        EbiObjectImporter::EventLog(CompressedEventLog::import_as_object)
    ],
};

pub struct CompressedEventLog {
    log: EventLog
}

impl CompressedEventLog {

    pub fn read_as_finite_language(reader: &mut dyn BufRead) -> Result<Box<dyn EbiTraitFiniteLanguage>> {
        let event_log = Self::import(reader)?;
        Ok(Box::new(event_log.log.to_finite_language()))
    }

    pub fn read_as_finite_stochastic_language(reader: &mut dyn BufRead) -> Result<Box<dyn EbiTraitFiniteStochasticLanguage>> {
        let event_log = Self::import(reader)?;
        Ok(Box::new(Into::<FiniteStochasticLanguage>::into(event_log.log.to_finite_stochastic_language())))
    }

    pub fn read_as_queriable_stochastic_language(reader: &mut dyn BufRead) -> Result<Box<dyn EbiTraitQueriableStochasticLanguage>> {
        let event_log = Self::import(reader)?;
        Ok(Box::new(event_log.log.to_finite_stochastic_language()))
    }

    pub fn read_as_iterable_stochastic_language(reader: &mut dyn BufRead) -> Result<Box<dyn EbiTraitIterableStochasticLanguage>> {
        let event_log = Self::import(reader)?;
        Ok(Box::new(Into::<FiniteStochasticLanguage>::into(event_log.log.to_finite_stochastic_language())))
    }

    pub fn read_as_event_log(reader: &mut dyn BufRead) -> Result<Box<dyn EbiTraitEventLog>> {
        let event_log = Self::import(reader)?;
        Ok(Box::new(event_log.log))
    }

    pub fn read_as_stochastic_deterministic_semantics(reader: &mut dyn BufRead) -> Result<EbiTraitStochasticDeterministicSemantics> {
        let mut event_log = EventLog::import(reader)?;
        let sdfa = event_log.to_stochastic_deterministic_finite_automaton();
        let semantics = StochasticDeterministicFiniteAutomaton::get_p_semantics(Rc::new(sdfa))?;
        Ok(EbiTraitStochasticDeterministicSemantics::Usize(semantics))
    }
}

impl Importable for CompressedEventLog {
    fn import_as_object(reader: &mut dyn BufRead) -> Result<EbiObject> {
        Ok(EbiObject::EventLog(Self::import(reader)?.log))
    }

    fn import(reader: &mut dyn BufRead) -> anyhow::Result<Self> where Self: Sized {
        let dec = GzDecoder::new(reader);
        let mut reader2 = BufReader::new(dec);
        let log = EventLog::import(&mut reader2)?;
        Ok(Self {
            log: log
        })
    }
}