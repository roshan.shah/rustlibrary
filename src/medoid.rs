use crate::{activity_key::ActivityKey, ebi_objects::finite_language::FiniteLanguage, ebi_traits::ebi_trait_finite_stochastic_language::EbiTraitFiniteStochasticLanguage, levenshtein, math::fraction::Fraction};
use anyhow::{anyhow, Result};
use fraction::{approx, One, Zero};

pub fn medoid(log: Box<dyn EbiTraitFiniteStochasticLanguage>, number_of_traces: &usize) -> Result<FiniteLanguage> {

    if number_of_traces.is_one() {
        let trace_number = medoid_single(&log);
        if trace_number.is_none() {
            return Err(anyhow!("1 trace was requested, but the stochastic language contains none"));
        }
        let mut result = FiniteLanguage::new();
        result.push(log.get(trace_number.unwrap()).unwrap().0.to_owned());
        return Ok(result);
    }

    if log.len() < *number_of_traces {
        return Err(anyhow!("{} traces were requested, but the stochastic language contains only {}", number_of_traces, log.len()));
    }

    log::info!("compute {} medoid traces", number_of_traces);

    let mut sum_distance = sum_distances(&log);

    let mut list = Vec::new();
    while list.len() < *number_of_traces {

        //find the position of the minimum value
        let mut min_pos = 0;
        for i in 1..sum_distance.len() {
            if sum_distance[i] < sum_distance[min_pos] {
                min_pos = i;
            }
        }

        //report the minimum value
        list.push(min_pos);
        sum_distance[min_pos] = Fraction::two();
    }
    list.sort();

    //put in the output format
    let mut result = FiniteLanguage::new();
    let mut list_i = 0;
    for (i1, (trace1, _)) in log.iter().enumerate() {
        if list_i < list.len() && i1 == list[list_i] {
            result.push(trace1.to_vec());
            list_i += 1;
        }
    }

    Ok(result)
}

pub fn medoid_single(log: &Box<dyn EbiTraitFiniteStochasticLanguage>) -> Option<usize> {
    if log.len() < 1 {
        return None;
    }

    let sum_distance = sum_distances(log);

    //report the minimum value
    let min_pos = sum_distance.into_iter().enumerate().fold((0, Fraction::two()), 
        |(old_index, old_min), (new_index, new_value)| {
                if new_value < old_min {(new_index, new_value)} else {(old_index, old_min)}});

    return Some(min_pos.0);
}

pub fn sum_distances(log: &Box<dyn EbiTraitFiniteStochasticLanguage>) -> Vec<Fraction> {
    let mut activity_key = ActivityKey::new();
    
    let mut sum_distance = vec![Fraction::zero(); log.len()];

    for (i1, (trace1, probability1)) in log.iter().enumerate() {

        let itrace1 = activity_key.process_trace(trace1);

        for (i2, (trace2, probability2)) in log.iter().enumerate().take(i1) {
            let itrace2 = activity_key.process_trace(trace2);

            let mut dist = levenshtein::normalised(&itrace1, &itrace2);

            dist *= probability1;
            dist *= probability2;
            
            sum_distance[i1] += &dist;
            sum_distance[i2] += dist;
        }
    }

    sum_distance
}

pub fn k_medoids_clustering(log: &Box<dyn EbiTraitFiniteStochasticLanguage>, number_of_clusters: &usize) -> Result<FiniteLanguage> {
    //https://dm.cs.tu-dortmund.de/en/mlbits/cluster-kmedoids-intro/
    //https://www.cs.umb.edu/cs738/pam1.pdf

    if log.len() < *number_of_clusters {
        return Err(anyhow!("{} clusters were requested, but the stochastic language contains only {} traces", number_of_clusters, log.len()));
    }
    
    // let dissim = Matrix::new();
    // let mut meds = kmedoids::random_initialization(4, *number_of_clusters, &mut rand::thread_rng());
    // let (loss, assingment, n_iter, n_swap): (Fraction, _, _, _) = kmedoids::fasterpam(&dissim, &mut meds, 100);
    // println!("Loss is: {}", loss);


    todo!()
}