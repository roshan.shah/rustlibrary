use std::{fmt::Display, io::BufRead, str::FromStr, hash::Hash};
use anyhow::{anyhow, Result, Error};

use crate::{ebi_commands::ebi_command::{EbiCommand2, EBI_COMMANDS}, ebi_input_output::EbiInput, ebi_objects::{compressed_event_log::EBI_COMPRESSED_EVENT_LOG, directly_follows_model::EBI_DIRCTLY_FOLLOWS_MODEL, ebi_object::EbiObject, event_log::EBI_EVENT_LOG, finite_language::EBI_FINITE_LANGUAGE, finite_stochastic_language::EBI_FINITE_STOCHASTIC_LANGUAGE, labelled_petri_net::EBI_LABELLED_PETRI_NET, stochastic_deterministic_finite_automaton::EBI_STOCHASTIC_DETERMINISTIC_FINITE_AUTOMATON, stochastic_labelled_petri_net::EBI_STOCHASTIC_LABELLED_PETRI_NET}, ebi_traits::ebi_trait::{EbiTrait, FromEbiTraitObject}, import::{EbiObjectImporter, EbiTraitImporter}};

pub const EBI_FILE_HANDLERS: &'static [EbiFileHandler] = &[
    EBI_FINITE_LANGUAGE,
    EBI_LABELLED_PETRI_NET,
    EBI_STOCHASTIC_LABELLED_PETRI_NET,
    EBI_FINITE_STOCHASTIC_LANGUAGE,
    EBI_STOCHASTIC_DETERMINISTIC_FINITE_AUTOMATON,
    EBI_EVENT_LOG,
    EBI_COMPRESSED_EVENT_LOG,
    EBI_DIRCTLY_FOLLOWS_MODEL
];

#[derive(Clone)]
pub struct EbiFileHandler {
    pub name: &'static str,
    pub article: &'static str, //a or an
    pub file_extension: &'static str,
    pub validator: fn(&mut dyn BufRead) -> Result<()>,
    pub trait_importers: &'static [EbiTraitImporter],
    pub object_importers: &'static [EbiObjectImporter]
}

impl FromStr for EbiFileHandler {
    type Err = Error;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        for file_handler in EBI_FILE_HANDLERS {
            if file_handler.name == s || file_handler.file_extension == s {
                return Ok(file_handler.clone());
            }
        }
        return Err(anyhow!("{} is not an Ebi file handler.", s));
    }
}

impl FromEbiTraitObject for EbiFileHandler {
    fn from_trait_object(object: crate::ebi_input_output::EbiInput) -> Result<Box<Self>> {
        match object {
            EbiInput::FileHandler(e) => Ok(Box::new(e)),
            _ => Err(anyhow!("cannot read {} {} as an integer", object.get_type().get_article(), object.get_type()))
        } 
    }
}

impl Eq for EbiFileHandler {}

impl PartialEq for EbiFileHandler {
    fn eq(&self, other: &Self) -> bool {
        self.name == other.name
    }
}

impl PartialOrd for EbiFileHandler {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.name.partial_cmp(&other.name)
    }
}

impl Ord for EbiFileHandler {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.name.cmp(&other.name)
    }
}

impl Hash for EbiFileHandler {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.name.hash(state);
    }
}

impl Display for EbiFileHandler {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.name)
    }
}