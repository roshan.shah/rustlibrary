use std::collections::HashMap;

use anyhow::Result;
use fraction::{One, Zero};
use crate::{activity_key::Activity, ebi_objects::{labelled_petri_net::LabelledPetriNet, stochastic_labelled_petri_net::StochasticLabelledPetriNet}, ebi_traits::{ebi_trait_finite_stochastic_language::EbiTraitFiniteStochasticLanguage, ebi_trait_iterable_stochastic_language::EbiTraitIterableStochasticLanguage, ebi_trait_labelled_petri_net::EbiTraitLabelledPetriNet}, math::fraction::Fraction};

pub fn mine(mut net: Box<dyn EbiTraitLabelledPetriNet>, language: Box<dyn EbiTraitFiniteStochasticLanguage>) -> StochasticLabelledPetriNet {
    let mut activity2frequency: HashMap<Activity, Fraction> = HashMap::new();

    for (trace, probability) in language.iter() {
        let utrace = net.get_activity_key().process_trace(trace);
        for activity in utrace {
            *activity2frequency.entry(activity).or_insert(Fraction::zero()) += probability.clone();
        }
    }

    let mut weights: Vec<Fraction> = vec![];
    for transition in net.get_transitions() {
        weights.push(
            if transition.is_silent() || !activity2frequency.contains_key(&transition.get_label()) {
                Fraction::one()
            } else {
                activity2frequency.get(&transition.get_label()).unwrap().clone()
            }
        );
    }

    (net, weights).into()
}