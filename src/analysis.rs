use std::{collections::HashMap, fmt::Display, fmt::Debug};
use core::hash::Hash;
use crate::{ebi_objects::finite_stochastic_language::FiniteStochasticLanguage, ebi_traits::ebi_trait_stochastic_deterministic_semantics::{EbiTraitStochasticDeterministicSemantics, StochasticDeterministicSemantics}, math::fraction::Fraction, Trace};
use anyhow::{anyhow, Result};
use fraction::{One, Zero};
use priority_queue::PriorityQueue;

pub trait PSemanticsAnalyser {
    fn analyse_minimum_probability(&self, at_least: &Fraction) -> Result<FiniteStochasticLanguage>;
    
    fn analyse_number_of_traces(&self, number_of_traces: &usize) -> Result<FiniteStochasticLanguage>;

    fn analyse_min_prob<FS: Hash + Display + Debug + Clone + Eq>(semantics: &dyn StochasticDeterministicSemantics<PState = FS>, at_least: &Fraction) -> Result<FiniteStochasticLanguage>;

    fn analyse_num_traces<FS: Hash + Display + Debug + Clone + Eq>(semantics: &dyn StochasticDeterministicSemantics<PState = FS>, number_of_traces: &usize) -> Result<FiniteStochasticLanguage>;
}

impl PSemanticsAnalyser for EbiTraitStochasticDeterministicSemantics {
    fn analyse_minimum_probability(&self, at_least: &Fraction) -> Result<FiniteStochasticLanguage> {
        match self {
            EbiTraitStochasticDeterministicSemantics::Usize(s) => {
                let semantics = s.as_ref();
                Self::analyse_min_prob(semantics, at_least)
            },
            EbiTraitStochasticDeterministicSemantics::PMarking(s) => {
                let semantics = s.as_ref();
                Self::analyse_min_prob(semantics, at_least)
            },
        }
    }

    fn analyse_number_of_traces(&self, number_of_traces: &usize) -> Result<FiniteStochasticLanguage> {
        match self {
            EbiTraitStochasticDeterministicSemantics::Usize(s) => {
                let semantics = s.as_ref();
                Self::analyse_num_traces(semantics, number_of_traces)
            },
            EbiTraitStochasticDeterministicSemantics::PMarking(s) => {
                let semantics = s.as_ref();
                Self::analyse_num_traces(semantics, number_of_traces)
            },
        }
    }

    fn analyse_min_prob<FS: Hash + Display + Debug + Clone + Eq>(semantics: &dyn StochasticDeterministicSemantics<PState = FS>, at_least: &Fraction) -> Result<FiniteStochasticLanguage> {
        log::info!("start minimum probability analysis");
        let mut result = HashMap::new();

        let mut queue = vec![];
        queue.push(X{
            prefix: vec![],
            probability: Fraction::one(),
            p_state: semantics.get_initial_state()?,
        });

        while let Some(x) = queue.pop() {
            // log::info!("queue length {}, process p-state {:?}", queue.len(), x.p_state);

            let termination_probability = &x.probability * semantics.get_termination_probability(&x.p_state);
            // log::info!("got termination probability {}", termination_probability);

            if termination_probability >= *at_least{
                result.insert(x.prefix.clone(), termination_probability);
            }

            if &x.probability * (Fraction::one() - semantics.get_termination_probability(&x.p_state)) >= *at_least {
                for activity in semantics.get_enabled_activities(&x.p_state) {
                    // log::info!("consider activity {}", activity);

                    let probability = semantics.get_activity_probability(&x.p_state, activity);

                    // log::info!("activity has probability {}", probability);

                    let new_p_state = semantics.execute_activity(&x.p_state, activity)?;

                    // log::info!("activity executed");

                    let mut new_prefix = x.prefix.clone();
                    new_prefix.push(semantics.get_activity_label(&activity).to_string());
                    let new_x = X {
                        prefix: new_prefix,
                        probability: &x.probability * probability,
                        p_state: new_p_state,
                    };

                    queue.push(new_x);
                }
            }
        }

        if result.is_empty() {
            return Err(anyhow!("Analsyis returned an empty language; there are no traces that have a probability of at least {}.", at_least));
        }

        Ok(FiniteStochasticLanguage::new(result))
    }

    fn analyse_num_traces<FS: Hash + Display + Debug + Clone + Eq>(semantics: &dyn StochasticDeterministicSemantics<PState = FS>, number_of_traces: &usize) -> Result<FiniteStochasticLanguage> {
        log::info!("start number of traces analysis");
        let mut result = HashMap::new();

        let mut queue = PriorityQueue::new();
        queue.push(Y::Prefix(vec![], semantics.get_initial_state()?), Fraction::one());

        while let Some((y, y_probability)) = queue.pop() {
            match y {
                Y::Prefix(prefix, p_state) => {

                    let termination_proability = semantics.get_termination_probability(&p_state);
                    if !termination_proability.is_zero() {
                        queue.push(Y::Trace(prefix.clone()), &y_probability * termination_proability);
                    }

                    for activity in semantics.get_enabled_activities(&p_state) {
                        let activity_probability = semantics.get_activity_probability(&p_state, activity);
        
                        let new_p_state = semantics.execute_activity(&p_state, activity)?;
                        let mut new_prefix = prefix.clone();
                        new_prefix.push(semantics.get_activity_label(&activity).to_string());
                        let new_probability = &y_probability * activity_probability;
        
                        queue.push(Y::Prefix(new_prefix, new_p_state), new_probability);
                    }
                },
                Y::Trace(trace) => {
                    // if trace.len() > 2 {
                        result.insert(trace, y_probability);
                        if result.len() >= *number_of_traces {
                            break;
                        }
                    // }
                },
            }
        }


        if result.is_empty() {
            return Err(anyhow!("Analysis returned an empty language; there are no traces in the model."));
        }

        Ok(FiniteStochasticLanguage::new(result))
    }
}

struct X<FS: Hash + Display + Debug + Clone + Eq> {
    prefix: Trace,
    probability: Fraction,
    p_state: FS
}

enum Y<FS: Hash + Display + Debug + Clone + Eq> {
    Prefix(Trace, FS),
    Trace(Trace)
}

impl <FS: Hash + Display + Debug + Clone + Eq> Eq for Y<FS> {}

impl <FS: Hash + Display + Debug + Clone + Eq> PartialEq for Y<FS> {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Prefix(l0, _), Self::Prefix(r0, _)) => l0 == r0,
            (Self::Trace(l0), Self::Trace(r0)) => l0 == r0,
            _ => false,
        }
    }
}

impl <FS: Hash + Display + Debug + Clone + Eq> Hash for Y<FS> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        match self {
            Y::Prefix(t, _) => t.hash(state),
            Y::Trace(t) => t.hash(state),
        }
    }
}