use std::{fmt::{self, Display}, fs::File, hash::Hash, io::Write, path::PathBuf};

use anyhow::{Result, Context};

use clap::ArgMatches;

use crate::math::{fraction::Fraction, log_div::LogDiv};

pub trait Displayable: Hash + Clone + Eq + Display + fmt::Debug {
    fn debug(&self) -> String;
}

pub trait Exportable {
    fn export(&self, f: &mut impl std::io::Write) -> Result<()>;
}

pub fn output_fraction(result: &Fraction) {
    println!("{}", result);
    println!("Approximately {:.4}", result);
}

pub fn output_logdiv(result: &LogDiv) {
    if result.is_exact() {
        println!("{}", result);
    }
    let a = result.approximate();
    match a {
        Ok(a) => println!("Approximately {:.4}", a),
        Err(_) => println!("no approximation available"),
    }
}

pub fn export_object(matches: &ArgMatches, option: &str, object: impl fmt::Display) -> Result<()> {
    if let Some(to_file) = matches.get_one::<PathBuf>(option) {
        log::info!("write object to {:?}", to_file);
        
        let file = File::create(to_file).with_context(|| format!("writing {} to file {:?}", option, to_file))?;
        let mut writer = std::io::BufWriter::new(&file);
        write!(writer, "{}", object).with_context(|| format!("writing {} to file {:?}", option, to_file))?;
        return writer.flush().with_context(|| format!("writing {} to file {:?}", option, to_file));
    } else {
        print!("{}", object);
        return Ok(());
    }
}

pub fn export_object2(to_file: &PathBuf, object: &impl Exportable) -> Result<()> {
    log::info!("Write result to {:?}", to_file);

    let file = File::create(to_file).with_context(|| format!("writing result to file {:?}", to_file))?;
    let mut writer = std::io::BufWriter::new(&file);
    object.export(&mut writer).with_context(|| format!("writing result to file {:?}", to_file))?;
    return writer.flush().with_context(|| format!("writing result to file {:?}", to_file));
}

pub fn export_to_string(object: &impl Exportable) -> Result<String> {
    let mut f = vec![];
    let text_result = object.export(&mut f)?;
    Ok(String::from_utf8(f)?)
}

impl Exportable for String {
    fn export(&self, f: &mut impl std::io::Write) -> Result<()> {
        Ok(writeln!(f, "{}", self)?)
    }
}

impl Exportable for usize {
    fn export(&self, f: &mut impl std::io::Write) -> Result<()> {
        Ok(writeln!(f, "{}", self)?)
    }
}