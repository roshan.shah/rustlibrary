use anyhow::{Result,anyhow};
use std::{collections::{HashMap,HashSet}, str::FromStr, borrow::Borrow};
use crate::{ebi_objects::stochastic_deterministic_finite_automaton::StochasticDeterministicFiniteAutomaton, ebi_traits::{ebi_trait_finite_stochastic_language::EbiTraitFiniteStochasticLanguage, ebi_trait_queriable_stochastic_language::EbiTraitQueriableStochasticLanguage}, math::{fraction::Fraction, log_div::LogDiv}};
use num_traits::ToPrimitive;


pub fn jssc_log2log(event_log1: Box<dyn EbiTraitFiniteStochasticLanguage>, event_log2: Box<dyn EbiTraitFiniteStochasticLanguage>) -> Result<LogDiv> {
    todo!()
}

pub fn jssc_log2model(log1: Box<dyn EbiTraitFiniteStochasticLanguage>, mut logmodel2: Box<dyn EbiTraitQueriableStochasticLanguage>) -> Result<LogDiv> {
    todo!()
}

pub fn jssc_model2model_with_random_sample(mut logmodel1: Box<dyn EbiTraitQueriableStochasticLanguage>, mut logmodel2: Box<dyn EbiTraitQueriableStochasticLanguage>, number_of_traces: usize
) -> Result<LogDiv>{
   todo!()
}