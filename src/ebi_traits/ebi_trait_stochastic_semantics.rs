use std::{fmt::Display, hash::Hash, io::BufRead, rc::Rc};
use anyhow::{anyhow, Result};

use crate::{activity_key::Activity, ebi_input_output::EbiInput, ebi_objects::ebi_object::EbiTraitObject, math::fraction::Fraction, stochastic_labelled_petri_net_semantics::SLPNMarking};

use super::ebi_trait::FromEbiTraitObject;

pub enum EbiTraitStochasticSemantics {
	Marking(Box<dyn StochasticSemantics<State = SLPNMarking>>),
	Usize(Box<dyn StochasticSemantics<State = usize>>)
}

impl FromEbiTraitObject for EbiTraitStochasticSemantics {
    fn from_trait_object(object: EbiInput) -> Result<Box<Self>> {
        match object {
            EbiInput::Trait(EbiTraitObject::StochasticSemantics(e)) => Ok(Box::new(e)),
            _ => Err(anyhow!("cannot read {} {} as a stochastic semantics", object.get_type().get_article(), object.get_type()))
        }
    }
}

pub trait StochasticSemantics {
	type State: Eq + Hash + Clone + Display;

    /**
	 * (Re)set the semantics to the initial state.
	 */
    fn get_initial_state(&self) -> Self::State;


    /**
	 * Update the state to reflect execution of the transition. This alters the state to avoid repeated memory allocations in simple walkthroughs.
	 * Will return an error when the transition is not enabled, or when the marking cannot be represented (unbounded).
	 * 
	 * @param transition
	 */
    fn execute_transition(&self, state: &mut Self::State, transition: TransitionIndex) -> Result<()>;

    /**
	 * 
	 * @return whether the current state is a final state.
	 */
    fn is_final_state(&self, state: &Self::State) -> bool;

    /**
	 * 
	 * @param transition
	 * @return the weight of the transition. This might depend on the state.
	 */
    fn get_transition_weight(&self, state: &Self::State, transition: TransitionIndex) -> &Fraction;

	fn is_transition_silent(&self, transition: TransitionIndex) -> bool;

	/**
	 * Only call if the transition is not silent. Otherwise, this may panic.
	 */
	fn get_transition_activity(&self, transition: TransitionIndex) -> Activity;

	/**
	 * Only call if the transition is not silent. Otherwise, this may panic.
	 */
	fn get_transition_label(&self, transition: TransitionIndex) -> &str;

	fn get_activity_label(&self, activity: &Activity) -> &str;

    /**
	 * 
	 * @param enabledTransitions
	 * @return the sum of the weight of the enabled transitions
	 */
    fn get_total_weight_of_enabled_transitions(&self, state: &Self::State) -> anyhow::Result<Fraction>;

	fn get_enabled_transitions(&self, state: &Self::State) -> Vec<TransitionIndex>;

}

pub type TransitionIndex = usize;

pub trait ToStochasticSemantics {
	type State: Eq + Hash + Clone + Display;
	fn get_stochastic_semantics(net: Rc<Self>) -> Box<dyn StochasticSemantics<State = Self::State>>;

	fn import_as_stochastic_semantics(reader: &mut dyn BufRead) -> Result<EbiTraitStochasticSemantics>;
}