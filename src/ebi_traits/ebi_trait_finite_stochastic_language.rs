use anyhow::{anyhow, Result};

use crate::{ebi_input_output::EbiInput, ebi_objects::ebi_object::EbiTraitObject, math::fraction::Fraction, Trace};

use super::{ebi_trait::FromEbiTraitObject, ebi_trait_iterable_stochastic_language::EbiTraitIterableStochasticLanguage};

pub trait EbiTraitFiniteStochasticLanguage : EbiTraitIterableStochasticLanguage {
    fn len(&self) -> usize;

    fn get(&self, trace_index: usize) -> Option<(&Trace, &Fraction)>;
}

impl FromEbiTraitObject for dyn EbiTraitFiniteStochasticLanguage {
    fn from_trait_object(object: EbiInput) -> Result<Box<Self>> {
        match object {
            EbiInput::Trait(EbiTraitObject::FiniteStochasticLanguage(e)) => Ok(e),
            _ => Err(anyhow!("cannot read {} {} as a finite stochastic language", object.get_type().get_article(), object.get_type()))
        }
    }
}