use std::{collections::hash_set::Iter, fmt::Display};

use anyhow::{anyhow, Result};

use crate::{ebi_input_output::EbiInput, ebi_objects::ebi_object::EbiTraitObject, Trace};

use super::ebi_trait::FromEbiTraitObject;

pub trait EbiTraitFiniteLanguage : Display {
    fn len(&self) -> usize;

    fn iter(&self) -> Iter<'_, Trace>;
}

impl FromEbiTraitObject for dyn EbiTraitFiniteLanguage {
    fn from_trait_object(object: EbiInput) -> Result<Box<Self>> {
        match object {
            EbiInput::Trait(EbiTraitObject::FiniteLanguage(e)) => Ok(e),
            _ => Err(anyhow!("cannot read {} {} as a finite language", object.get_type().get_article(), object.get_type()))
        }
    }
}