use anyhow::{anyhow, Result};
use crate::{activity_key::ActivityKey, ebi_input_output::EbiInput, ebi_objects::ebi_object::{EbiObject, EbiTraitObject}, marking::Marking, net::Transition};

use super::ebi_trait::FromEbiTraitObject;


pub trait EbiTraitLabelledPetriNet {
    fn get_activity_key(&mut self) -> &mut ActivityKey;

    fn get_number_of_places(&self) -> usize;

    fn get_number_of_transitions(&self) -> usize;

    fn get_transitions(&self) -> &Vec<Transition>;

    fn get_initial_marking(&self) -> &Marking;
}

impl FromEbiTraitObject for dyn EbiTraitLabelledPetriNet {
    fn from_trait_object(object: EbiInput) -> Result<Box<Self>> {
        match object {
            EbiInput::Object(EbiObject::LabelledPetriNet(e)) => Ok(Box::new(e)),
            _ => Err(anyhow!("Cannot read {} {} as a labelled Petri net.", object.get_type().get_article(), object.get_type()))
        }
    }
}