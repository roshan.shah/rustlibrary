use std::collections::HashMap;

use anyhow::{anyhow, Result};

use crate::{activity_key::{Activity, ActivityKey}, ebi_commands::ebi_command_info::Infoable, ebi_input_output::EbiInput, ebi_objects::{ebi_object::EbiTraitObject, event_log::DataType}, Trace};

use super::ebi_trait::FromEbiTraitObject;

pub trait EbiTraitEventLog  {
    fn get_log(&self) -> &process_mining::EventLog;

    fn gather_trace_attributes(&self) -> HashMap<String, DataType>;

    fn get_number_of_traces(&self) -> usize;

    fn traces(&self) -> &Vec<process_mining::event_log::Trace>;

    fn read_trace(&mut self, trace_index: &usize) -> Vec<Activity>; //get the trace in numerical form

    fn read_trace_with_activity_key(&self, activity_key: &mut ActivityKey, trace_index: &usize) -> Vec<Activity>; //get the trace in numerical form with the specified activity key
}

impl dyn EbiTraitEventLog {
    pub fn to_multiset(&mut self) -> HashMap<Vec<Activity>, usize> {
        let mut map = HashMap::new();
        for trace_index in 0..self.get_number_of_traces() {
            let trace = self.read_trace(&trace_index);
            match map.entry(trace) {
                std::collections::hash_map::Entry::Occupied(mut e) => {*e.get_mut() += 1;()},
                std::collections::hash_map::Entry::Vacant(e) => {e.insert(1);()},
            }
        }
        map
    }
}

impl FromEbiTraitObject for dyn EbiTraitEventLog {
    fn from_trait_object(object: EbiInput) -> Result<Box<Self>> {
        match object {
            EbiInput::Trait(EbiTraitObject::EventLog(e)) => Ok(e),
            _ => Err(anyhow!("cannot read {} {} as an event log", object.get_type().get_article(), object.get_type()))
        }
    }
}