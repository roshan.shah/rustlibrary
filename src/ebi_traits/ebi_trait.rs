use std::{collections::HashSet, fmt::{Debug, Display}, path::PathBuf};
use anyhow::{anyhow, Result};
use clap::{builder::ValueParser, value_parser};

use crate::{ebi_input_output::EbiInput, ebi_objects::ebi_object::{EbiObjectType, EbiTraitObject}, file_handler::{EbiFileHandler, EBI_FILE_HANDLERS}, math::fraction::Fraction};

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum EbiTrait {
    EventLog,
    FiniteLanguage,
    FiniteStochasticLanguage,
    IterableStochasticLanguage,
    QueriableStochasticLanguage,
    StochasticDeterministicSemantics,
    StochasticSemantics,
    LabelledPetriNet,
}

impl EbiTrait {
    /**
     * Get all file handlers that can import to this trait.
     */
    pub fn get_file_handlers(&self) -> Vec<&EbiFileHandler> {
        let mut result = HashSet::new();
        for file_handler in EBI_FILE_HANDLERS {
            for importer in file_handler.trait_importers {
                if &importer.get_trait() == self {
                    result.insert(file_handler);
                }
            }
        };

        let mut result: Vec<&EbiFileHandler> = result.into_iter().collect();
        result.sort();
        result
    }

    pub fn get_article(&self) -> &str {
        match self {
            EbiTrait::EventLog => "an",
            EbiTrait::FiniteLanguage => "a",
            EbiTrait::FiniteStochasticLanguage => "a",
            EbiTrait::IterableStochasticLanguage => "an",
            EbiTrait::QueriableStochasticLanguage => "a",
            EbiTrait::StochasticDeterministicSemantics => "a",
            EbiTrait::StochasticSemantics => "a",
            EbiTrait::LabelledPetriNet => "a",
        }
    }
}

impl Display for EbiTrait {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self {
            EbiTrait::EventLog => "event log",
            EbiTrait::FiniteLanguage => "finite language",
            EbiTrait::FiniteStochasticLanguage => "finite stochastic language",
            EbiTrait::IterableStochasticLanguage => "iterable stochastic language",
            EbiTrait::QueriableStochasticLanguage => "queriable stochastic language",
            EbiTrait::StochasticDeterministicSemantics => "stochastic deterministic semantics",
            EbiTrait::StochasticSemantics => "stochastic semantics",
            EbiTrait::LabelledPetriNet => "labelled Petri net",
        })
    }
}

impl Debug for EbiTrait {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        std::fmt::Display::fmt(&self, f)
    }
}

pub trait FromEbiTraitObject {
    fn from_trait_object(object: EbiInput) -> Result<Box<Self>>;
}

impl FromEbiTraitObject for usize {
    fn from_trait_object(object: EbiInput) -> Result<Box<Self>> {
        match object {
            EbiInput::Usize(e) => Ok(Box::new(e)),
            _ => Err(anyhow!("cannot read {} {} as an integer", object.get_type().get_article(), object.get_type()))
        } 
    }
}

impl FromEbiTraitObject for String {
    fn from_trait_object(object: EbiInput) -> Result<Box<Self>> {
        match object {
            EbiInput::String(e) => Ok(Box::new(e)),
            _ => Err(anyhow!("cannot read {} {} as an integer", object.get_type().get_article(), object.get_type()))
        } 
    }
}