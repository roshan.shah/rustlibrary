use std::collections::hash_map::Iter;

use anyhow::{anyhow, Result};

use crate::{ebi_input_output::EbiInput, ebi_objects::ebi_object::EbiTraitObject, math::fraction::Fraction, Trace};

use super::ebi_trait::FromEbiTraitObject;

pub trait EbiTraitIterableStochasticLanguage { //an iterable language is not necessarily finite
    fn iter(&self) -> Box<dyn EbiTraitStochasticLanguageIterator + '_>;
}

pub trait EbiTraitStochasticLanguageIterator<'a> : Iterator<Item = (&'a Trace, &'a Fraction)> {

}

impl FromEbiTraitObject for dyn EbiTraitIterableStochasticLanguage {
    fn from_trait_object(object: EbiInput) -> Result<Box<Self>> {
        match object {
            EbiInput::Trait(EbiTraitObject::IterableStochasticLanguage(e)) => Ok(e),
            _ => Err(anyhow!("cannot read {} {} as an iterable stochastic language", object.get_type().get_article(), object.get_type()))
        }
    }
}