use crate::{activity_key::Activity, ebi_input_output::EbiInput, ebi_objects::ebi_object::EbiTraitObject, math::fraction::Fraction, p_semantics_for_stochastic_semantics::PMarking, stochastic_labelled_petri_net_semantics::SLPNMarking};
use anyhow::{anyhow, Result};

use super::ebi_trait::FromEbiTraitObject;

pub enum EbiTraitStochasticDeterministicSemantics {
	Usize(Box<dyn StochasticDeterministicSemantics<PState = usize>>),
    PMarking(Box<dyn StochasticDeterministicSemantics<PState = PMarking<SLPNMarking>>>)
}

impl FromEbiTraitObject for EbiTraitStochasticDeterministicSemantics {
    fn from_trait_object(object: EbiInput) -> Result<Box<Self>> {
        match object {
            EbiInput::Trait(EbiTraitObject::StochasticDeterministicSemantics(e)) => Ok(Box::new(e)),
            _ => Err(anyhow!("cannot read {} {} as a stochastic deterministic semantics", object.get_type().get_article(), object.get_type()))
        }
    }
}

pub trait StochasticDeterministicSemantics {
    type PState;

    /**
	 * (Re)set the semantics to the initial state.
	 */
    fn get_initial_state(&self) -> Result<Self::PState>;


    /**
     * Get the state that results from executing the activity. 
     * This method should not be called on activities that are not enabled or have a zero proability in this state.
    */
    fn execute_activity(&self, state: &Self::PState, activity: Activity) -> Result<Self::PState>;

    /**
     * 
    * @return whether the current state is a final state.
    */
    fn get_termination_probability(&self, state: &Self::PState) -> Fraction;

    /**
     * 
    * @param activity
    * @return the probability of the activity in the given state.
    */
    fn get_activity_probability(&self, state: &Self::PState, activity: Activity) -> Fraction;

    fn get_activity_label(&self, activity: &Activity) -> &str;

    fn get_enabled_activities(&self, state: &Self::PState) -> Vec<Activity>;

}

